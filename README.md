# Fracture mechanics module # {#um_fracture_module_readme}

# Installation

For details look to MoFEM installation page located [here](http://mofem.eng.gla.ac.uk/mofem/html/installation.html).

# Update to current version 

## Version 0.12.4

Installation for version version `v0.12.4` follows the same patter like for 
other versions
```
cd $HOME/mofem_install 
curl -s -L https://api.github.com/repos/likask/spack/tarball/Version0.12.4 | \
tar xzC spack --strip 1 && \
spack install mofem-fracture-module@0.12.4
spack view -verbose symlink -i um_view_0.12.4 mofem-fracture-module@0.12.4
echo "export PATH=$PWD/um_view_0.12.4/bin:\$PATH" >> ~/.bashrc
```

## Version 0.11.0

Follow similar procedure to set for version `v0.10.0`

1. Go to directory where MoFEM is installed:

```
cd $HOME/mofem_install 
```
2. Install new Fracture Module version, i.e. `v0.11.0`:

```
curl -s -L https://api.github.com/repos/likask/spack/tarball/Version0.11.0 | \
tar xzC spack --strip 1 && \
spack install mofem-fracture-module@0.11.0
```
3. Create view
```
spack view -verbose symlink -i um_view_0.11.0 mofem-fracture-module@0.11.0
```
4. Update path
```
echo "export PATH=$PWD/um_view_0.11.0/bin:\$PATH" >> ~/.bashrc
```
## Version 0.10.0

The following steps require that:
- MoFEM is installed using `install_mofem_user.sh` script.
- Directory `mofem_install` is in home directory
- Installation is for Linux (on macOS use `~/.bash_profile` instead of `~/.bashrc`)

1. Go to directory where MoFEM is installed:

```
cd $HOME/mofem_install 
```
2. Install new Fracture Module version, i.e. `v0.10.0`:

```
curl -s -L https://api.github.com/repos/likask/spack/tarball/Version0.10.0 | \
tar xzC spack --strip 1 && \
spack install mofem-fracture-module@0.10.0
```
3. Create view
```
spack view -verbose symlink -i um_view_0.10.0 mofem-fracture-module@0.10.0
```
4. Update path
```
echo "export PATH=$PWD/um_view_0.10.0/bin:\$PATH" >> ~/.bashrc
```

___Note___: You can install MoFEM in root `/opt` directory, so that multiple users can make a view to steps 3 and 4 above and therefore share the MoFEM installation. In order to do that, you must update Spack environment, for example in case of installation in `/opt` directory, as follows:
```
. /opt/mofem_install/spack/share/spack/setup-env.sh
export PATH=/opt/mofem_install/um_view/fracture_mechanics/um_view/bin:$PATH
```

Alternatively, you can also install MoFEM in your home directory, and give other users read permission to your `$HOME/mofem_install/spack` directory. That will enable them to view your fracture module installation and use the code.

