#!/bin/bash
# This script reads the log file then extracts result data and 
# plots relation graphs in MoFEM fracture module.
# Plot setttings can be changed by modifying the gnuplot script get_graphs.gnu
#
# Input:
#           log file named 'log'
# 
# Output:
#           pdf graphs
#           displacement_loadFactor_crackArea_energy.csv

# Help.
if [[ "$1" == "-h" || "$1" == "--help" ]]; then
  echo 'Plotting script for MoFEM Fracture Module
        Usage:
          plotting_data.sh              Create and open graphs from log file
          plotting_data.sh -s           Create only, do not open graphs. Silent
          plotting_data.sh -d           Delete all output files of non-converged steps
          plotting_data.sh -d out_skin  Delete out_skin* files of non-converged steps
        '
  exit 0
fi

# Create raw data file of displacement - loadFactor - crack area - energy
# from log file (append 'TODELETE' to end of line with 'Not Converged')
grep -E "(Propagation step|F_lambda2|Crack surface area)" log | \
perl -pe 's/\e\[[0-9;]*m//g' | \
awk '{$1=""; $2=""; sub("  ", " "); print}' | awk '{$1=$1;print}' | \
sed 'N; s/\nCrack//;P;D'| sed '/Not Converged/ s/$/ TODELETE/' | \
sed 's/Not Converged //g'| \
awk 'BEGIN  {print "-1 0 0 0 0 0"} /F_lambda2 / { lambda = $NF } \
/Propagation step/ { F=lambda; print $3 "\t" 2*$9/F "\t" F "\t" $13 "\t" $9 "\t" $NF}'| \
tee data_raw.dat

# Keep unique updated data only (in case of restart from previous steps)
cat data_raw.dat | sed '1!G;h;$!d' | sort -u -n | tee unique_analysis_steps.dat

# Remove unused last column data 'TODELETE', and write to a text file
cat unique_analysis_steps.dat | grep -v "TODELETE" | \
sed 's/[[:space:]]\{1,\}[^[:space:]]\{1,\}$//' | \
tee displacement_loadFactor_crackArea_energy.txt

# Format columns
printf "%14s  %14s  %14s  %14s  %14s\n" \
$(cat displacement_loadFactor_crackArea_energy.txt) > temp_file; \
mv temp_file displacement_loadFactor_crackArea_energy.txt

# Make a csv copy
awk '{print $1","$2","$3","$4","$5}' displacement_loadFactor_crackArea_energy.txt| \
tee displacement_loadFactor_crackArea_energy.csv

# Print final data file to console
cat displacement_loadFactor_crackArea_energy.txt

# Remove text file
rm displacement_loadFactor_crackArea_energy.txt

# Make graphs using gnuplot. Note: ignore the first row of zeros when plotting
# crack area
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
gnuplot -e "load '$DIR/get_graphs.gnu'; quit"

# Convert graphs on .ps to .pdf
for i in `ls *.ps`; do
    ps2pdf $i;
    rm $i;
    # echo 'Converted $i into pdf, deleted original file.';
done

# Delete output files of non-converged steps if '-d' option is used
if  [[ $1 = "-d" ]]; then
    echo -e "\nDeleting output files $2* associated with non-converged steps"
    cat unique_analysis_steps.dat | grep TODELETE | awk '{print $1}' | \
    tee non_converged_steps.txt
    while read p; do
        # echo "Non-converged step: $p"
        rm -rf $2*_$p.*
    done <non_converged_steps.txt
    
fi

# Open pdf graphs
machine="$(uname -s)"
# echo ${machine}
if [[ ${machine} = "Darwin" && "$1" != "-s" ]]
then
    # echo "This is a Mac computer"
    open graph_*.pdf
else
    echo "PDF graphs are ready to open."
fi

# Remove unnecessary files
rm -f *.dat