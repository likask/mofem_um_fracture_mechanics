#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()


#reader = LegacyVTKReader('/Users/user/mofem/users_modules/fracture_mechanics/out_spatial0.vtk'

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# Properties modified on calculator1
calculator1 = Calculator(Input=GetActiveSource())
SetActiveSource(calculator1)
if any("SINGULAR_DISP" in str for str in calculator1.PointData.keys()):
 calculator1.Function = 'MESH_NODE_POSITIONS+SINGULAR_DISP-coords'
else:
 calculator1.Function = 'MESH_NODE_POSITIONS-coords'
calculator1.ResultArrayName = 'Result1'

# show nad hide data in view
calculator1Display = Show(calculator1, renderView1)
Hide(calculator1, renderView1)
# SetActiveSource(calculator1)
# create a new 'Warp By Vector'
WarpByVector1 = WarpByVector(Input=calculator1)
WarpByVector1.Vectors = [None, 'Result1']
# create a new 'Calculator'
calculator2 = Calculator(Input=WarpByVector1)


# Properties modified on calculator2
if any("SINGULAR_DISP" in str for str in calculator2.PointData.keys()):
  calculator2.Function = 'SPATIAL_POSITION-MESH_NODE_POSITIONS-SINGULAR_DISP'
else:
  calculator2.Function = 'SPATIAL_POSITION-MESH_NODE_POSITIONS'
calculator2.ResultArrayName = 'DISPLACEMENTS'
SetActiveSource(calculator2)
# create a new 'Warp By Vector'
# update the view to ensure updated data information
calculator2Display = Show(calculator2, renderView1)
Hide(calculator2, renderView1)
WarpByVector2 = WarpByVector(Input=calculator2)
WarpByVector2.Vectors = [None, 'DISPLACEMENTS']
WarpByVector2.ScaleFactor = 1.0
WarpByVector2Display = Show(WarpByVector2, renderView1)
