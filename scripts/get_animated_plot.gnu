# This gnuplot script generates series of png plots which then be used to create
# animated plots

reset

data_file = "displacement_loadFactor_crackArea_energy.csv"

# Get number of data entries
stats data_file
num_rows = STATS_records

# Setup png plotting
set terminal pngcairo size 1360,910 enhanced font ',20'
set datafile separator ","

# Color definitions
set style line 1 lc rgb "blue" lt 1 lw 5 pt 7 ps 2.5

unset key
set xlabel "Crack area"
set ylabel "Load factor" 
set xrange [0:60000]
set yrange [0:4]
set arrow 1 dt 2 lw 2 lc rgb "red" from graph 0,first 1 to graph 1,first 1 nohead

system('mkdir -p png')

# Generate png plots
n=0
do for [ii=1:num_rows] {
    n=n+1
    set output sprintf('png/animated_plot_%03.0f.png',n)
    plot data_file every ::1::ii u 4:(sqrt($3**2)) w l ls 1, \
          data_file every ::ii::ii u 4:(sqrt($3**2)) w p ls 1
}

# command line: gnuplot -e "load 'get_animated_plot.gnu'; quit"