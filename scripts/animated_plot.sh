#!/bin/bash
# This script generates a animated plot movie using series of png plots. 
# Default plot is crack area against load factor. Plot setttings can be changed 
# by modifying the gnuplot script get_animated_plot.gnu
# 
# Input:
#           displacement_loadFactor_crackArea_energy.csv
# 
# Output:
#           animated_plot.mp4
# 
# Usage:
#       1. Change working directory to the place where csv file is located. 
#           This csv file is generated after running plotting_data.sh
#           
#       2. Make sure animated_plot.sh and get_animated_plot.gnu are accessible 
#           from the working directory. This can be done by either copying 
#           those two files to the working directory or adding them to the same 
#           PATH (in .bashrc or .bash_profile)
# 
#       3. Make sure package ffmpeg is installed and loaded by a package 
#           manager, for example, spack.
# 
#       4. If needed, modify parameter 'fps' in animated_plot.sh to the desired
#           number of frames per second for the output movie
# 
#       5. Run animated_plot.sh
#
#       6. Obtain animated movie, animated_plot.mp4, in the same directory. 
#           Movie will be opened automatically if macOS is used.

# Generate png plots
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
gnuplot -e "load '$DIR/get_animated_plot.gnu'; quit"

# Load ffmpeg, if already installed using spack
# spack load ffmpeg

# Frame per second
fps=45

# Generate animated plot
ffmpeg -y -f image2 -r $fps -i 'png/animated_plot_%03d.png' animated_plot.mp4

# Delete generated png plots
rm -R png

# Open animated plot
machine="$(uname -s)"
# echo ${machine}
if [ ${machine} = "Darwin" ]
then
    # echo "This is a Mac computer"
    open animated_plot.mp4
else
    echo "Animated plot is ready to open"
fi