# This gnuplot script generates .ps graphs for displacement - load factor, 
# crack area - energy, and crack area - load factor relations

set terminal postscript color   # eps enhanced "Verdana" 12
set datafile separator ","

#
# DISPLACEMENT - LOAD FACTOR
#

set output "graph_displacement_loadFactor.ps"

set xlabel "Displacement"
set ylabel "Load factor"

unset key
set key right # box

data_file = "displacement_loadFactor_crackArea_energy.csv"

plot data_file u (sqrt($2**2)):(sqrt($3**2)) \
title "Displacement vs. Load factor" w lp pt 7 ps 0.8 lc rgb "blue" \
lw 5 dt 1 pointinterval 1


#
# LOAD FACTOR - ENERGY
#

set output "graph_energy_loadFactor.ps"

set xlabel "Load factor"
set ylabel "Elastic energy"

unset key
set key right # box

data_file = "displacement_loadFactor_crackArea_energy.csv"

plot data_file u (sqrt($3**2)):($5*1) \
title "Load factor vs. Energy" w lp pt 7 ps 0.8 lc rgb "blue" \
lw 5 dt 1 pointinterval 1

#
# CRACK AREA - ENERGY
#
set output "graph_crackArea_energy.ps"

set xlabel "Crack area"
set ylabel "Elastic energy"

unset key
set key right # box

# Ignoring the first row with zero crack area
plot data_file every ::1 u 4:($5*1) \
title "Crack area vs. Elastic energy" w lp pt 7 ps 0.8 lc rgb "blue" \
lw 5 dt 1 pointinterval 1


#
# CRACK AREA - LOAD FACTOR
# 
set output "graph_crackArea_loadFactor.ps"

set xlabel "Crack area"
set ylabel "Load factor"

unset key
set key right # box
set arrow 1 dt 2 lw 2 lc rgb "red" from graph 0,first 1 to graph 1,first 1 nohead

# set xrange [0:10]
set yrange [0:]

# Ignoring the first row with zero crack area
plot data_file every ::1 u 4:(sqrt($3**2)) \
title "Crack area vs. Load factor" w lp pt 7 ps 0.8 lc rgb "blue" \
lw 5 dt 1 pointinterval 1

# Plot data from specific row range (e.g. 2-793)
# plot "<(sed -n '2,793p'  displacement_loadFactor_crackArea_energy.txt)"  u 4:(sqrt($3**2)) \
# title "Crack area vs. Load factor" w lp pt 7 ps 0.8 lc rgb "blue" \
# lw 5 dt 1 pointinterval 1
