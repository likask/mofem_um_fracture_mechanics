set grid mytics ytics mxtics xtics
set logscale x
set logscale y

set title "Convergence plots for crack release energy (exact solution gc = 1e-5)"
set xlabel "number of dgrees of freedom"
set ylabel "(g-gc)/gc in %"

plot \
'gp_h_o2' w lp lt 1 lw 2 pt 1 t "coarse-local-h-o2 (no sigularity)",\
'gp_h_o2_m20' w lp lt 1 lw 2 pt 4 t "middle-local-h-o2 (no sigularity)",\
'gp_h_o2_m50' w lp lt 1 lw 2 pt 3 t "fine-local-h-o2 (no sigularity)",\
'gp_p_o2' w lp lt 2 lw 2 pt 1 t "coarse-local-p-o2",\
'gp_p_o3' w lp lt 2 lw 2 pt 4 t "coarse-local-p-o3",\
'gp_p_o4' w lp lt 2 lw 2 pt 2 t "coarse-local-p-o4",\
'gp_p_o2_m20' w lp lt 3 lw 2 pt 1 t "middle-local-p-o2",\
'gp_p_o3_m20' w lp lt 3 lw 2 pt 4 t "middle-local-p-o3",\
'gp_p_o4_m20' w lp lt 3 lw 2 pt 2 t "middle-local-p-o4",\
'gp_p_o2_m50' w lp lt 4 lw 2 pt 1 t "fine-local-p-o2",\
'gp_p_o3_m50' w lp lt 4 lw 2 pt 4 t "fine-local-p-o3",\
'gp_p_o4_m50' w lp lt 4 lw 2 pt 2 t "fine-local-p-o4"
