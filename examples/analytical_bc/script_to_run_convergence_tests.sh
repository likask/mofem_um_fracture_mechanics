
rm -f gp
FILE=examples/analytical_bc/out_10.h5m

for r in `seq 0 0`; do

  mpirun -np 4 ./crack_propagation \
  -my_file $FILE  \
  -snes_monitor -ksp_monitor \
  -ksp_type gmres \
  -ksp_atol 1e-12 -ksp_rtol 0 \
  -my_order 2 -my_geom_order 2 \
  -my_ref 1 -my_ref_order 1 \
  -my_add_singularity 1 -se_refined_integration 1 -se_number_of_refinement_levels 4 \
  -material HOOKE \
  -mofem_mg_coarse_order 1 -mofem_mg_levels 2 \
  -mofem_mg_verbose 1 -mg_coarse_ksp_type preonly \
  -mg_coarse_pc_type lu \
  -pc_mg_smoothup 10 -pc_mg_smoothdown 10 -pc_mg_galerkin true -pc_mg_type multiplicative \
  -pc_type lu \
  -my_max_post_proc_ref_level 0 \
  -test analytical_mode_1 -test_tol 1e-3 \
  -2>&1 | tee log

  awk '
  BEGIN {sum=0; conunt=0}
  /griffith force at ent/ {
    if(sqrt($7*$7)<=0.4) {
      sum+=$11;
      count+=1
    }
  }
  /name ELASTIC Nb. local dof/ { nbdofs=$25 }
  END {
    g = sum/count;
    gc = 1e-5;
    e = sqrt((g-gc)^2)*100;
    print nbdofs,e/gc
  }
  ' log | tee -a gp

done
