### Fracture Module v0.12.0

- Support for taking into account eigen positions (resulting from eigen stress) in contact elements
- Possibility for elastic analysis step with closed crack
- Possibility for single step for elastic analysis without cutting the mesh
- Support for new implementation of pcomm
- Fixed memory leaks
- Other improvements

----

### Fracture Module v0.11.0

- Add functionality for crack propagation near and on surfaces with springs attached (ALE formulation)
- Add functionality for an interface which is constrained during mesh cutting and crack propagation
- Add functionality to limit the crack to a given blockset
- Various minor bug fixes and improvements

----

### Fracture Module v0.10.0

- Mortar (non-matching meshes) contact functionality
- Update of CMakeLists.txt to accommodate integration of Mortar Contact module
- Updates to reflect CORE an UM developments improving memory and run-time efficiency
- Robustness improvements for crack crossing fixed edges
- Robustness improvements for crack propagation along matching-meshes contact interface
- Fixes to the data plotting script to reflect changes in logging
- Refactoring of the logging code
- Various minor bug fixes

----

### Fracture Module v0.9.62

- Crack propagation along pressure surface (ALE)
- Crack propagation along contact interface (ALE)
- Integration of new cutting algorithm
- Adaptive MWLS algorithm
- MWLS error estimators
- MWLS stress projection on cells
- New tests (including SSLV116 and memcheck)
- Fixes of memory leaks
- Code refactoring and bug fixes