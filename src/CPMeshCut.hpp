/** \file CPCutMesh.hpp
  \brief Cutting mesh
*/

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#ifndef __CP_CUTMESH_HPP__
#define __CP_CUTMESH_HPP__

namespace FractureMechanics {

struct CPMeshCut : public MoFEM::UnknownInterface {

  /**
   * \brief Getting interface of core database
   * @param  uuid  unique ID of interface of CPMeshCut solely
   * @param  iface returned pointer to interface
   * @return       error code
   */
  MoFEMErrorCode query_interface(boost::typeindex::type_index type_index,
                                 MoFEM::UnknownInterface **iface) const;

  CrackPropagation &cP;
  CPMeshCut(CrackPropagation &cp);
  virtual ~CPMeshCut() {}

  /**
   * @brief Get options from command line
   *
   * @return MoFEMErrorCode
   */
  MoFEMErrorCode getOptions();

  /**
   * @brief Set cutting volume from bit ref level
   *
   * @param bit
   * @return MoFEMErrorCode
   */
  MoFEMErrorCode setVolume(const BitRefLevel &bit);

  /**
   * @brief Refine mesh close to crack surface and crack front
   *
   * @param verb
   * @param vol pointer to volumes to be refined (can be set to null if set
   * somwere else)
   * @param make_front make crack front from skin surface
   * @param update_front update crack surface information only in vicinity of
   * crack front
   * @param debug
   * @return MoFEMErrorCode
   */
  MoFEMErrorCode refineMesh(const Range *vol, const bool make_front,
                            const int verb = QUIET, const bool debug = false);

  /**
   * @brief Set the cut surface from file
   *
   * Load cutting mesh from file.
   *
   * @return MoFEMErrorCode
   */
  MoFEMErrorCode setCutSurfaceFromFile();

  MoFEMErrorCode copySurface(const std::string save_mesh = "");

  MoFEMErrorCode rebuildCrackSurface(const double factor,
                                     const std::string save_mesh = "",
                                     const int verb = QUIET,
                                     bool debug = false);

  /**
   * @brief Ref, cut and trim, merge and do TetGen, if option is on.
   *
   * This is running cut mesh interface function
   *
   * @param first_bit
   * @param debug
   * @return MoFEMErrorCode
   */
  MoFEMErrorCode cutMesh(int &first_bit, const bool debug = false);

  inline const Range &getCuttingSurface() const { return sUrface; }

  /// \brief find body skin
  MoFEMErrorCode findBodySkin(const BitRefLevel bit, const int verb = 1,
                              const bool debug = false,
                              std::string file_name = "out_body_skin.vtk");
  /// \brief get crack front
  MoFEMErrorCode findCrack(const BitRefLevel bit, const int verb = 1,
                           const bool debug = false);

  /// \brief get crack front
  MoFEMErrorCode findCrackFromPrisms(const BitRefLevel bit, const int verb = 1,
                                     const bool debug = false);

  /// \brief get contact elements
  MoFEMErrorCode findContactFromPrisms(const BitRefLevel bit,
                                       const int verb = 1,
                                       const bool debug = false);

  /// \brief split crack faces
  MoFEMErrorCode splitFaces(const int verb = 1, const bool debug = false);

  /// \brief insert contact interface
  MoFEMErrorCode insertContactInterface(const int verb = 1,
                                        const bool debug = false);

  /// \brief insert contact interface
  MoFEMErrorCode addMortarContactPrisms(const int verb = 1,
                                        const bool debug = false);

  /// \brief refine elements at crack tip
  MoFEMErrorCode refineCrackTip(const int front_id, const int verb = 1,
                                const bool debug = false);

  MoFEMErrorCode refineAndSplit(const int verb = 1, const bool debug = false);

  /**
   * @brief Refine, cut, trim, merge and ref front and split
   *
   * \note This functions squashes bits, and do refine of crack front after mesh
   * is cutted. That refinement improves approximation of the fields but
   * not geometry of crack front.
   *
   * @param verb
   * @param debug
   * @return MoFEMErrorCode
   */
  MoFEMErrorCode cutRefineAndSplit(const int verb = VERBOSE,
                                   bool debug = false);

  /// \brief get crack front edges and finite elements
  MoFEMErrorCode getFrontEdgesAndElements(const BitRefLevel bit,
                                          const int verb = 1,
                                          const bool debug = false);
  MoFEMErrorCode setFixEdgesAndCorners(const BitRefLevel bit);

  std::vector<double> &getOrgCoords() { return originalCoords; }
  const std::vector<double> &getOrgCoords() const { return originalCoords; }

  inline const Range &getFixedEdges() const { return fixedEdges; }
  inline const Range &getCornerNodes() const { return cornerNodes; }
  inline const std::string &getCutSurfMeshName() const {
    return cutSurfMeshName;
  }
  inline const int getEdgesBlockSet() const {
    return edgesBlockSetFlg == PETSC_TRUE ? edgesBlockSet : 0;
  }

  MoFEMErrorCode getInterfacesPtr();

  int getCuttingSurfaceSidesetId() const { return cuttingSurfaceSidesetId; };
  int getSkinOfTheBodyId() const { return skinOfTheBodyId; }
  int getCrackSurfaceId() const { return crackSurfaceId; };
  int getCrackFrontId() const { return crackFrontId; };
  int getContactPrismsBlockSetId() const { return contactPrismsBlockSetId; };

  MoFEMErrorCode clearData();

private:
  MoFEMErrorCode setMeshOrgCoords();
  MoFEMErrorCode getMeshOrgCoords();

  PetscBool edgesBlockSetFlg;
  int edgesBlockSet;
  int vertexBlockSet;
  int vertexNodeSet;
  int crackedBodyBlockSetId;
  int contactPrismsBlockSetId;

  CutMeshInterface *cutMeshPtr;
  BitRefManager *bitRefPtr;
  MeshsetsManager *meshsetMngPtr;

  Range fixedEdges;
  Range cornerNodes;
  Range fRont;
  Range sUrface;

  int fractionLevel;
  double tolCut;
  double tolCutClose;
  double tolTrimClose;
  int nbRefBeforCut;
  int nbRefBeforTrim;

  PetscBool debugCut;
  PetscBool removePathologicalFrontTris;

  double sHift[3];
  int cuttingSurfaceSidesetId;
  int skinOfTheBodyId;
  int crackSurfaceId;
  int crackFrontId;

  std::vector<double> originalCoords;
  std::string cutSurfMeshName;
};
} // namespace FractureMechanics

#endif //__CP_CUTMESH_HPP__