/** \file CPSolvers.hpp
  \brief Solvers for crack propagation 
*/

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#ifndef __CP_SOLVERS_HPP__
#define __CP_SOLVERS_HPP__

namespace FractureMechanics {


/**
 * Crack propagation solvers 
 */
struct CPSolvers : MoFEM::UnknownInterface {

  /**
   * \brief Getting interface of core database
   * @param  uuid  unique ID of interface of CPSolvers solely
   * @param  iface returned pointer to interface
   * @return       error code
   */
  MoFEMErrorCode query_interface(boost::typeindex::type_index type_index,
                                 MoFEM::UnknownInterface **iface) const;

  CrackPropagation &cP;
  CPSolvers(CrackPropagation &cp);
  virtual ~CPSolvers() {}

  MoFEMErrorCode getOptions();

  /**
   * @brief Solve elastic problem
   * 
   * @param dm_elastic 
   * @return MoFEMErrorCode 
   */
  MoFEMErrorCode solveElastic(DM dm_elastic, Vec init,
                              const double load_factor);

  /**
   * @brief Solve for energy realease rate
   *
   * @param dm_material_forces
   * @param dm_surface_projection
   * @param dm_crack_srf_area
   * @param surface_ids
   * @return MoFEMErrorCode
   */
  MoFEMErrorCode calculateGc(DM dm_material_forces, DM dm_surface_projection,
                             DM dm_crack_srf_area,
                             const std::vector<int> &surface_ids);

  /**
   * @brief Solve crack propagation problem
   *
   * @param  dm_crack_propagation  composition of elastic DM and material DM
   * @param  dm_elastic            elastic DM
   * @param  dm_material           material DM
   * @param  dm_material_forces    used to calculate material forces, sub-dm of
   * dm_crack_propagation
   * @param  dm_surface_projection dm to project material forces on surfaces,
   * sub-dm of dm_crack_propagation
   * @param  dm_crack_srf_area     dm to project material forces on crack
   * surfaces, sub-dm of dm_crack_propagation
   * @param  surface_ids           IDs of surface meshsets
   * @param  cut_mesh_it     number of catting steps
   * @param  set_cut_surface     flag to cut surfaces
   *
   * @return MoFEMErrorCode
   */
  MoFEMErrorCode
  solvePropagation(DM dm_crack_propagation, DM dm_elastic, DM dm_material,
                   DM dm_material_forces, DM dm_surface_projection,
                   DM dm_crack_srf_area, const std::vector<int> &surface_ids,
                   int cut_mesh_it = 0, const bool set_cut_surface = false);

  /**
   * @brief Solve cutting mesh problem
   * @param  dm_crack_propagation  composition of elastic DM and material DM
   * @param  dm_elastic            elastic DM
   * @param  dm_eigen_elastic      eigen elastic DM
   * @param  dm_material           material DM
   * @param  dm_material_forces    used to calculate material forces, sub-dm of
   * dm_crack_propagation
   * @param  dm_surface_projection dm to project material forces on surfaces,
   * sub-dm of dm_crack_propagation
   * @param  dm_crack_srf_area     dm to project material forces on crack
   * surfaces, sub-dm of dm_crack_propagation
   * @param  surface_ids           IDs of surface meshsets
   * @param  debug     flag for debugging
   *
   * @return MoFEMErrorCode
   */
  MoFEMErrorCode solveCutMesh(SmartPetscObj<DM> &dm_crack_propagation,
                              SmartPetscObj<DM> &dm_elastic,
                              SmartPetscObj<DM> &dm_eigen_elastic,
                              SmartPetscObj<DM> &dm_material,
                              SmartPetscObj<DM> &dm_material_forces,
                              SmartPetscObj<DM> &dm_surface_projection,
                              SmartPetscObj<DM> &dm_crack_srf_area,
                              std::vector<int> &surface_ids,
                              const bool debug = false);
};
}

#endif // __CP_SOLVERS_HPP__

