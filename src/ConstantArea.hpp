/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#ifndef __CONSTANT_AREA_HPP__
#define __CONSTANT_AREA_HPP__

#ifndef WITH_ADOL_C
#error "MoFEM need to be compiled with ADOL-C"
#endif

namespace FractureMechanics {

/** \brief Constant area constrains
 */
struct ConstantArea {

  MoFEM::Interface &mField;
  ConstantArea(MoFEM::Interface &m_field)
      : mField(m_field), commonData(m_field) {}

  struct CommonData {

    MoFEM::Interface &mField;
    CommonData(MoFEM::Interface &m_field) : mField(m_field) {}

    VectorDouble crackAreaIncrement;
    MatrixDouble tangentCrackAreaIncrement;
    vector<double *> tangentCrackAreaIncrementRowPtr;

    VectorDouble crackFrontTangent;
    MatrixDouble tangentCrackFrontTangent;
    vector<double *> tangentCrackFrontTangentRowPtr;

    map<int, Vec> mapV;
    bool setMapV;

    Tag thInterfaceSide;
  };
  CommonData commonData;

  struct MyTriangleFE : public FaceElementForcesAndSourcesCore {

    CommonData &commonData;
    std::string fieldName;

    Mat petscB;
    Vec petscF;
    Mat petscQ;

    MyTriangleFE(MoFEM::Interface &m_field, CommonData &common_data,
                 std::string field_name = "LAMBDA_CRACKFRONT_AREA")
        : FaceElementForcesAndSourcesCore(m_field), commonData(common_data),
          fieldName(field_name), petscB(PETSC_NULL), petscF(PETSC_NULL),
          petscQ(PETSC_NULL) {}
    int getRule(int order) { return order; };

    PetscErrorCode preProcess() {
      MoFEMFunctionBegin;
      CHKERR FaceElementForcesAndSourcesCore::preProcess();

      if (petscB != PETSC_NULL) {
        snes_B = petscB;
      }

      if (petscF != PETSC_NULL) {
        snes_f = petscF;
      }

      switch (ts_ctx) {
      case CTX_TSSETIFUNCTION: {
        if (!petscF) {
          snes_ctx = CTX_SNESSETFUNCTION;
          snes_f = ts_F;
        }
        break;
      }
      case CTX_TSSETIJACOBIAN: {
        if (!petscB) {
          snes_ctx = CTX_SNESSETJACOBIAN;
          snes_B = ts_B;
        }
        break;
      }
      default:
        break;
      }

      // Tag to detect which side is it
      CHKERR mField.get_moab().tag_get_handle("INTERFACE_SIDE",
                                              commonData.thInterfaceSide);

      // If petscQ matrix is defined appply projection to each row
      if (petscQ != PETSC_NULL) {
        if (petscB == PETSC_NULL) {
          SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                  "Matrix B should be set");
        }
        const auto field_bit_number = mField.get_field_bit_number(fieldName);
        for (_IT_NUMEREDDOF_ROW_BY_BITNUMBER_FOR_LOOP_(problemPtr, field_bit_number,
                                                  dit)) {
          CHKERR mField.getInterface<VecManager>()->vecCreateGhost(
              problemPtr->getName(), COL,
              &commonData.mapV[dit->get()->getPetscGlobalDofIdx()]);
          CHKERR VecZeroEntries(
              commonData.mapV[dit->get()->getPetscGlobalDofIdx()]);
          CHKERR VecGhostUpdateBegin(
              commonData.mapV[dit->get()->getPetscGlobalDofIdx()],
              INSERT_VALUES, SCATTER_FORWARD);
          CHKERR VecGhostUpdateEnd(
              commonData.mapV[dit->get()->getPetscGlobalDofIdx()],
              INSERT_VALUES, SCATTER_FORWARD);
        }
        commonData.setMapV = true;
      } else {
        commonData.setMapV = false;
      }

      MoFEMFunctionReturn(0);
    }

    PetscErrorCode postProcess() {
      MoFEMFunctionBegin;
      CHKERR FaceElementForcesAndSourcesCore::postProcess();
      if (petscQ != PETSC_NULL) {
        if (petscB == PETSC_NULL) {
          SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                  "Matrix B should be set");
        }

        // Q vectors
        void *void_ctx;
        CHKERR MatShellGetContext(petscQ, &void_ctx);
        ConstrainMatrixCtx *q_mat_ctx = (ConstrainMatrixCtx *)void_ctx;

        Vec Qv;
        CHKERR mField.getInterface<VecManager>()->vecCreateGhost(
            problemPtr->getName(), COL, &Qv);
        CHKERR MatAssemblyBegin(petscB, MAT_FLUSH_ASSEMBLY);
        CHKERR MatAssemblyEnd(petscB, MAT_FLUSH_ASSEMBLY);
        auto project_vectors_and_assemble = [this, Qv](auto dit) {
          MoFEMFunctionBegin;
          if (commonData.mapV.find(dit->get()->getPetscGlobalDofIdx()) ==
              commonData.mapV.end()) {
            SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                    "Vector on row not found");
          }
          CHKERR VecAssemblyBegin(
              commonData.mapV[dit->get()->getPetscGlobalDofIdx()]);
          CHKERR VecAssemblyEnd(
              commonData.mapV[dit->get()->getPetscGlobalDofIdx()]);
          CHKERR VecGhostUpdateBegin(
              commonData.mapV[dit->get()->getPetscGlobalDofIdx()], ADD_VALUES,
              SCATTER_REVERSE);
          CHKERR VecGhostUpdateEnd(
              commonData.mapV[dit->get()->getPetscGlobalDofIdx()], ADD_VALUES,
              SCATTER_REVERSE);
          CHKERR VecGhostUpdateBegin(
              commonData.mapV[dit->get()->getPetscGlobalDofIdx()],
              INSERT_VALUES, SCATTER_FORWARD);
          CHKERR VecGhostUpdateEnd(
              commonData.mapV[dit->get()->getPetscGlobalDofIdx()],
              INSERT_VALUES, SCATTER_FORWARD);

          CHKERR MatMult(
              petscQ, commonData.mapV[dit->get()->getPetscGlobalDofIdx()], Qv);
          CHKERR VecGhostUpdateBegin(Qv, INSERT_VALUES, SCATTER_FORWARD);
          CHKERR VecGhostUpdateEnd(Qv, INSERT_VALUES, SCATTER_FORWARD);

          if (0) {
            auto save_vec = [](std::string name, Vec v) {
              PetscViewer viewer;
              PetscViewerASCIIOpen(PETSC_COMM_WORLD, name.c_str(), &viewer);
              PetscViewerPushFormat(viewer, PETSC_VIEWER_ASCII_MATLAB);
              VecView(v, viewer);
              PetscViewerPopFormat(viewer);
              PetscViewerDestroy(&viewer);
            };
            save_vec(
                "V_" + boost::lexical_cast<std::string>(dit->get()->getEnt()) +
                    ".m",
                commonData.mapV[dit->get()->getPetscGlobalDofIdx()]);
            save_vec(
                "Qv_" + boost::lexical_cast<std::string>(dit->get()->getEnt()) +
                    ".m",
                Qv);
          }

          if (dit->get()->getPart() == mField.get_comm_rank()) {
            double *array;
            CHKERR VecGetArray(Qv, &array);
            vector<int> glob_idx;
            vector<double> vals;
            int row = dit->get()->getPetscGlobalDofIdx();
            for (_IT_NUMEREDDOF_COL_BY_BITNUMBER_FOR_LOOP_(
                     problemPtr,
                     mField.get_field_bit_number("MESH_NODE_POSITIONS"),
                     diit)) {
              int idx = diit->get()->getPetscGlobalDofIdx();
              double val = array[diit->get()->getPetscGlobalDofIdx()];
              glob_idx.push_back(idx);
              vals.push_back(val);
            }
            CHKERR MatSetValues(petscB, 1, &row, glob_idx.size(),
                                &*glob_idx.begin(), &*vals.begin(), ADD_VALUES);
            CHKERR VecRestoreArray(Qv, &array);
          }

          MoFEMFunctionReturn(0);
        };

        for (_IT_NUMEREDDOF_ROW_BY_BITNUMBER_FOR_LOOP_(
                 problemPtr, mField.get_field_bit_number(fieldName), dit)) {
          CHKERR project_vectors_and_assemble(dit);
          CHKERR VecDestroy(
              &commonData.mapV[dit->get()->getPetscGlobalDofIdx()]);
        }

        CHKERR VecDestroy(&Qv);
        commonData.mapV.clear();
      }

      MoFEMFunctionReturn(0);
    }
  };

  boost::shared_ptr<MyTriangleFE> feRhsPtr;
  boost::shared_ptr<MyTriangleFE> feLhsPtr;

  struct OpAreaJacobian
      : public FaceElementForcesAndSourcesCore::UserDataOperator {

    int tAg;
    CommonData &commonData;
    bool isTapeRecorded;

    OpAreaJacobian(int tag, CommonData &common_data)
        : FaceElementForcesAndSourcesCore::UserDataOperator(
              "MESH_NODE_POSITIONS", UserDataOperator::OPROW),
          tAg(tag), commonData(common_data), isTapeRecorded(false) {
      sYmm = false;
    }

    /**

    \f[
    A = \| \mathbf{N} \| =
      \left\|
        \textrm{Spin}\left[
          \frac{\partial\mathbf{N}\mathbf{X}}{\partial\xi}
        \right]
        \frac{\partial\mathbf{N}\mathbf{X}}{\partial\eta}
        \right\|
    \f]

    */
    template <typename TYPE> struct AuxFunctions {

      MatrixDouble N, NTN;
      MatrixDouble Bksi;
      MatrixDouble Beta;

      ublas::vector<TYPE> referenceCoords;

      ublas::vector<TYPE> referenceXdKsi;
      ublas::vector<TYPE> referenceXdEta;
      ublas::matrix<TYPE> referenceSpinKsi;
      ublas::matrix<TYPE> referenceSpinEta;
      ublas::vector<TYPE> referenceNormal;
      TYPE referenceArea;

      ublas::vector<TYPE> currentCoords;
      ublas::vector<TYPE> currentXdKsi;
      ublas::vector<TYPE> currentXdEta;
      ublas::matrix<TYPE> currentSpinKsi;
      ublas::matrix<TYPE> currentSpinEta;
      ublas::vector<TYPE> currentNormal;
      TYPE currentArea;

      ublas::matrix<TYPE> A;
      ublas::vector<TYPE> crackAreaIncrement;
      ublas::vector<TYPE> crackFrontTangent;

      PetscErrorCode sPin(ublas::matrix<TYPE> &spin, ublas::vector<TYPE> &vec) {
        MoFEMFunctionBeginHot;
        spin.resize(3, 3, false);
        std::fill(spin.data().begin(), spin.data().end(), 0);
        spin(0, 1) = -vec[2];
        spin(0, 2) = +vec[1];
        spin(1, 0) = +vec[2];
        spin(1, 2) = -vec[0];
        spin(2, 0) = -vec[1];
        spin(2, 1) = +vec[0];
        MoFEMFunctionReturnHot(0);
      }

      PetscErrorCode matrixB(int gg, DataForcesAndSourcesCore::EntData &data) {
        MoFEMFunctionBeginHot;
        Bksi.resize(3, 9, false);
        Bksi.clear();
        for (int ii = 0; ii < 3; ++ii) {
          for (int jj = 0; jj < 3; ++jj) {
            Bksi(jj, ii * 3 + jj) = data.getDiffN(gg)(ii, 0);
          }
        }
        Beta.resize(3, 9, false);
        Beta.clear();
        for (int ii = 0; ii < 3; ++ii) {
          for (int jj = 0; jj < 3; ++jj) {
            Beta(jj, ii * 3 + jj) = data.getDiffN(gg)(ii, 1);
          }
        }
        MoFEMFunctionReturnHot(0);
      }

      PetscErrorCode dIffX() {
        MoFEMFunctionBeginHot;
        currentXdKsi.resize(3, false);
        currentXdEta.resize(3, false);
        std::fill(currentXdKsi.begin(), currentXdKsi.end(), 0);
        std::fill(currentXdEta.begin(), currentXdEta.end(), 0);
        for (auto ii = 0; ii != Bksi.size1(); ++ii)
          for (auto jj = 0; jj != Bksi.size2(); ++jj)
            currentXdKsi(ii) += Bksi(ii, jj) * currentCoords(jj);
        for (auto ii = 0; ii != Beta.size1(); ++ii)
          for (auto jj = 0; jj != Beta.size2(); ++jj)
            currentXdEta(ii) += Beta(ii, jj) * currentCoords(jj);
        // noalias(currentXdKsi) = prod(Bksi, currentCoords);
        // noalias(currentXdEta) = prod(Beta, currentCoords);
        MoFEMFunctionReturnHot(0);
      }

      PetscErrorCode nOrmal() {
        MoFEMFunctionBeginHot;

        CHKERR sPin(currentSpinKsi, currentXdKsi);
        CHKERR sPin(currentSpinEta, currentXdEta);
        currentNormal.resize(3, false);
        std::fill(currentNormal.begin(), currentNormal.end(), 0);
        for (auto ii = 0; ii != currentSpinKsi.size1(); ++ii)
          for (auto jj = 0; jj != currentSpinKsi.size2(); ++jj)
            currentNormal(ii) +=
                0.5 * currentSpinKsi(ii, jj) * currentXdEta(jj);
        // noalias(currentNormal) = 0.5 * prod(currentSpinKsi, currentXdEta);
        currentArea = 0;
        for (int dd = 0; dd != 3; ++dd) {
          currentArea += currentNormal[dd] * currentNormal[dd];
        }
        currentArea = sqrt(currentArea);
        MoFEMFunctionReturnHot(0);
      }

      PetscErrorCode matrixA() {
        MoFEMFunctionBeginHot;
        A.resize(3, 9, false);
        A.clear();
        for (auto ii = 0; ii != A.size1(); ++ii)
          for (auto jj = 0; jj != A.size2(); ++jj)
            for (auto kk = 0; kk != currentSpinKsi.size2(); ++kk)
              A(ii, jj) += 0.5 * (currentSpinKsi(ii, kk) * Beta(kk, jj) -
                                  currentSpinEta(ii, kk) * Bksi(kk, jj));
        // noalias(A) =
        //     0.5 * (prod(currentSpinKsi, Beta) - prod(currentSpinEta, Bksi));
        MoFEMFunctionReturnHot(0);
      }

      PetscErrorCode calculateAreaIncrementDirection(double beta) {
        MoFEMFunctionBeginHot;
        for (int dd = 0; dd != 9; dd++) {
          for (int ii = 0; ii != 3; ii++) {
            crackAreaIncrement[dd] +=
                beta * A(ii, dd) * currentNormal[ii] / currentArea;
          }
        }
        MoFEMFunctionReturnHot(0);
      }

      // FIXME Need to implement this with antisymmetric version need fix in
      // FTensor

      FTensor::Tensor2<TYPE, 3, 3> tSpin;

      PetscErrorCode calculateFrontTangent(double beta) {
        MoFEMFunctionBeginHot;
        FTensor::Index<'i', 3> i;
        FTensor::Index<'j', 3> j;
        tSpin(i, j) = 0;
        tSpin(0, 1) = -currentNormal[2];
        tSpin(0, 2) = +currentNormal[1];
        tSpin(1, 0) = +currentNormal[2];
        tSpin(1, 2) = -currentNormal[0];
        tSpin(2, 0) = -currentNormal[1];
        tSpin(2, 1) = +currentNormal[0];
        tSpin(i, j) /= beta * currentArea;
        crackFrontTangent.resize(9, false);
        FTensor::Tensor1<TYPE *, 3> t_crack_front_tangent(
            &crackFrontTangent[0], &crackFrontTangent[1], &crackFrontTangent[2],
            3);
        FTensor::Tensor1<TYPE *, 3> t_area_increment(&crackAreaIncrement[0],
                                                     &crackAreaIncrement[1],
                                                     &crackAreaIncrement[2], 3);
        for (int ii = 0; ii != 3; ++ii) {
          t_crack_front_tangent(i) = tSpin(i, j) * t_area_increment(j);
          ++t_area_increment;
          ++t_crack_front_tangent;
        }
        MoFEMFunctionReturnHot(0);
      }
    };

    AuxFunctions<adouble> a_auxFun;

    PetscErrorCode doWork(int side, EntityType type,
                          DataForcesAndSourcesCore::EntData &data) {
      MoFEMFunctionBegin;

      if (isTapeRecorded) {
        MoFEMFunctionReturnHot(0);
      }
      if (type != MBVERTEX) {
        MoFEMFunctionReturnHot(0);
      }

      int nb_dofs = data.getFieldData().size();
      int nb_gauss_pts = data.getN().size1();

      a_auxFun.referenceCoords.resize(9, false);
      a_auxFun.currentCoords.resize(9, false);

      trace_on(tAg);

      for (int dd = 0; dd != nb_dofs; dd++) {
        a_auxFun.referenceCoords[dd] <<= getCoords()[dd];
      }
      for (int dd = 0; dd != nb_dofs; dd++) {
        a_auxFun.currentCoords[dd] <<= data.getFieldData()[dd];
      }

      a_auxFun.crackAreaIncrement.resize(9, false);
      std::fill(a_auxFun.crackAreaIncrement.begin(),
                a_auxFun.crackAreaIncrement.end(), 0);

      for (int gg = 0; gg != nb_gauss_pts; ++gg) {

        double val = getGaussPts()(2, gg) * 0.5;

        CHKERR a_auxFun.matrixB(gg, data);
        CHKERR a_auxFun.dIffX();
        CHKERR a_auxFun.nOrmal();
        CHKERR a_auxFun.matrixA();
        CHKERR a_auxFun.calculateAreaIncrementDirection(val);
      }

      // cerr << "Griffith Force " << auxFun.griffithForce << endl;

      commonData.crackAreaIncrement.resize(nb_dofs, false);
      for (int dd = 0; dd != nb_dofs; dd++) {
        a_auxFun.crackAreaIncrement[dd] >>= commonData.crackAreaIncrement[dd];
      }

      trace_off();

      isTapeRecorded = true;

      MoFEMFunctionReturn(0);
    }
  };

  struct AuxOp {

    int tAg;
    CommonData &commonData;
    AuxOp(int tag, CommonData &common_data)
        : tAg(tag), commonData(common_data) {}

    ublas::vector<int> rowIndices;
    VectorDouble activeVariables;

    PetscErrorCode
    setVariables(FaceElementForcesAndSourcesCore::UserDataOperator *fe_ptr,
                 int side, EntityType type,
                 DataForcesAndSourcesCore::EntData &data) {
      MoFEMFunctionBegin;
      int nb_dofs = data.getIndices().size();
      activeVariables.resize(18);
      for (int dd = 0; dd != nb_dofs; dd++) {
        activeVariables[dd] = fe_ptr->getCoords()[dd];
      }
      for (int dd = 0; dd != nb_dofs; dd++) {
        activeVariables[9 + dd] = data.getFieldData()[dd];
      }
      MoFEMFunctionReturn(0);
    }
  };

  struct OpAreaC : public FaceElementForcesAndSourcesCore::UserDataOperator,
                   AuxOp {

    OpAreaC(int tag, CommonData &common_data)
        : FaceElementForcesAndSourcesCore::UserDataOperator(
              "LAMBDA_CRACKFRONT_AREA", "MESH_NODE_POSITIONS",
              UserDataOperator::OPROWCOL),
          AuxOp(tag, common_data) {
      sYmm = false;
    }

    MatrixDouble cMat;

    PetscErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type,
                          DataForcesAndSourcesCore::EntData &row_data,
                          DataForcesAndSourcesCore::EntData &col_data) {
      MoFEMFunctionBegin;

      if (row_type != MBVERTEX) {
        MoFEMFunctionReturnHot(0);
      }
      VectorInt &row_indices = row_data.getIndices();
      const int nb_rows = row_indices.size();
      if (!nb_rows)
        MoFEMFunctionReturnHot(0);
      VectorInt &col_indices = col_data.getIndices();
      const int nb_cols = col_indices.size();
      if (!nb_cols)
        MoFEMFunctionReturnHot(0);

      const EntityHandle *conn = getConn();
      for (int nn = 0; nn != 3; ++nn) {
        if (row_indices[nn] != -1) {
          if (row_data.getFieldDofs()[nn]->getEnt() != conn[nn]) {
            SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                    "Data inconsistency");
          }
        }
        for (int dd = 0; dd != 3; ++dd) {
          if (col_indices[3 * nn + dd] != -1) {
            if (col_data.getFieldDofs()[3 * nn + dd]->getEnt() != conn[nn]) {
              SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                      "Data inconsistency");
            }
          }
        }
      }

      auto row_dofs = getFEMethod()->getRowDofsPtr();
      for (auto it = row_dofs->begin(); it != row_dofs->end(); ++it) {
        int side = it->get()->getSideNumber();
        if (conn[side] != it->get()->getEnt()) {
          SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                  "Data inconsistency");
        }
      }

      auto col_dofs = getFEMethod()->getColDofsPtr();
      for (auto it = col_dofs->begin(); it != col_dofs->end(); ++it) {
        int side = it->get()->getSideNumber();
        if (conn[side] != it->get()->getEnt()) {
          SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                  "Data inconsistency");
        }
      }

      cMat.resize(3, 9, false);
      cMat.clear();

      CHKERR setVariables(this, col_side, col_type, col_data);

      int nb_dofs = col_data.getFieldData().size();
      commonData.crackAreaIncrement.resize(nb_dofs, false);
      int r;
      // play recorder for values
      r = ::function(tAg, nb_dofs, 18, &activeVariables[0],
                     &commonData.crackAreaIncrement[0]);
      if (r < 3) {
        SETERRQ1(PETSC_COMM_SELF, MOFEM_OPERATION_UNSUCCESSFUL,
                 "ADOL-C function evaluation with error r = %d", r);
      }

      auto check_isnormal = [](double v) {
        MoFEMFunctionBeginHot;
        if (v != 0 && !std::isnormal(v)) {
          SETERRQ1(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                   "Not a number %3.4e", v);
        }
        MoFEMFunctionReturnHot(0);
      };

      for (int rr = 0; rr != nb_rows; rr++) {
        if (row_indices[rr] != -1) {
          CHKERR check_isnormal(commonData.crackAreaIncrement[3 * rr + 0]);
          CHKERR check_isnormal(commonData.crackAreaIncrement[3 * rr + 1]);
          CHKERR check_isnormal(commonData.crackAreaIncrement[3 * rr + 2]);
          cMat(rr, 3 * rr + 0) = commonData.crackAreaIncrement[3 * rr + 0];
          cMat(rr, 3 * rr + 1) = commonData.crackAreaIncrement[3 * rr + 1];
          cMat(rr, 3 * rr + 2) = commonData.crackAreaIncrement[3 * rr + 2];
        }
      }

      if (!commonData.setMapV) {
        CHKERR MatSetValues(getFEMethod()->snes_B, nb_rows, &row_indices[0],
                            nb_cols, &col_indices[0], &cMat(0, 0), ADD_VALUES);
      } else {
        for (int rr = 0; rr != nb_rows; ++rr) {
          if (row_indices[rr] != -1) {
            if (commonData.mapV.find(row_indices[rr]) ==
                commonData.mapV.end()) {
              SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                      "Vector for given DOF in map not found");
            }
            for (int cc = 0; cc != nb_cols; ++cc) {
              if (col_indices[cc] != -1) {
                CHKERR check_isnormal(cMat(rr, cc));
              }
            }
            CHKERR VecSetValues(commonData.mapV[row_indices[rr]], nb_cols,
                                &col_indices[0], &cMat(rr, 0), ADD_VALUES);
          }
        }
      }

      MoFEMFunctionReturn(0);
    }
  };

  struct OpTangentJacobian : public OpAreaJacobian {

    OpTangentJacobian(int tag, CommonData &common_data)
        : OpAreaJacobian(tag, common_data) {
      sYmm = false;
    }

    PetscErrorCode doWork(int side, EntityType type,
                          DataForcesAndSourcesCore::EntData &data) {
      MoFEMFunctionBegin;

      if (isTapeRecorded) {
        MoFEMFunctionReturnHot(0);
      }
      if (type != MBVERTEX) {
        MoFEMFunctionReturnHot(0);
      }

      int nb_dofs = data.getFieldData().size();
      int nb_gauss_pts = data.getN().size1();

      a_auxFun.referenceCoords.resize(9, false);
      a_auxFun.currentCoords.resize(9, false);

      trace_on(tAg);

      for (int dd = 0; dd != nb_dofs; dd++) {
        a_auxFun.referenceCoords[dd] <<= getCoords()[dd];
      }
      for (int dd = 0; dd != nb_dofs; dd++) {
        a_auxFun.currentCoords[dd] <<= data.getFieldData()[dd];
      }

      a_auxFun.crackAreaIncrement.resize(9, false);
      a_auxFun.crackFrontTangent.resize(9, false);
      std::fill(a_auxFun.crackAreaIncrement.begin(),
                a_auxFun.crackAreaIncrement.end(), 0);
      std::fill(a_auxFun.crackFrontTangent.begin(),
                a_auxFun.crackFrontTangent.end(), 0);

      for (int gg = 0; gg != nb_gauss_pts; gg++) {

        double val = getGaussPts()(2, gg) * 0.5;

        CHKERR a_auxFun.matrixB(gg, data);
        CHKERR a_auxFun.dIffX();
        CHKERR a_auxFun.nOrmal();
        CHKERR a_auxFun.matrixA();
        CHKERR a_auxFun.calculateAreaIncrementDirection(1);
        CHKERR a_auxFun.calculateFrontTangent(val);
      }

      commonData.crackFrontTangent.resize(nb_dofs, false);
      for (int dd = 0; dd != nb_dofs; dd++) {
        a_auxFun.crackFrontTangent[dd] >>= commonData.crackFrontTangent[dd];
      }

      trace_off();

      isTapeRecorded = true;

      MoFEMFunctionReturn(0);
    }
  };

  struct OpTangentC : public FaceElementForcesAndSourcesCore::UserDataOperator,
                      AuxOp {

    OpTangentC(int tag, CommonData &common_data)
        : FaceElementForcesAndSourcesCore::UserDataOperator(
              "LAMBDA_CRACKFRONT_AREA_TANGENT", "MESH_NODE_POSITIONS",
              UserDataOperator::OPROWCOL),
          AuxOp(tag, common_data) {
      sYmm = false;
    }

    MatrixDouble cMat;

    PetscErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type,
                          DataForcesAndSourcesCore::EntData &row_data,
                          DataForcesAndSourcesCore::EntData &col_data) {
      MoFEMFunctionBegin;

      if (row_type != MBVERTEX) {
        MoFEMFunctionReturnHot(0);
      }

      VectorInt &row_indices = row_data.getIndices();
      const int nb_rows = row_indices.size();
      if (!nb_rows)
        MoFEMFunctionReturnHot(0);
      VectorInt &col_indices = col_data.getIndices();
      const int nb_cols = col_indices.size();
      if (!nb_cols)
        MoFEMFunctionReturnHot(0);
      cMat.resize(3, 9, false);
      cMat.clear();

      CHKERR setVariables(this, col_side, col_type, col_data);

      int nb_dofs = col_data.getFieldData().size();
      commonData.crackFrontTangent.resize(nb_dofs, false);
      int r;
      // play recorder for values
      r = ::function(tAg, nb_dofs, 18, &activeVariables[0],
                     &commonData.crackFrontTangent[0]);
      if (r < 3) {
        SETERRQ1(PETSC_COMM_SELF, MOFEM_OPERATION_UNSUCCESSFUL,
                 "ADOL-C function evaluation with error r = %d", r);
      }

      // Set face orientation
      {
        // Get orientation in respect to adjacent tet
        const EntityHandle face = getFEEntityHandle();
        const BitRefLevel bit = getFEMethod()->problemPtr->getBitRefLevel();
        Range adj_side_elems;
        CHKERR commonData.mField.getInterface<BitRefManager>()->getAdjacencies(
            bit, &face, 1, 3, adj_side_elems);
        adj_side_elems = adj_side_elems.subset_by_type(MBTET);
        if (adj_side_elems.size() != 1) {
          SETERRQ1(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                   "expect 1 tet but is %u", adj_side_elems.size());
        }
        EntityHandle side_elem = *adj_side_elems.begin();
        int side_number, sense, offset;
        CHKERR commonData.mField.get_moab().side_number(
            side_elem, face, side_number, sense, offset);
        if (sense == -1) {
          commonData.crackFrontTangent *= -1;
        }
        // Get orientaton of face in respect to face in the prism
        int side;
        CHKERR commonData.mField.get_moab().tag_get_data(
            commonData.thInterfaceSide, &face, 1, &side);
        if (side == 1) {
          commonData.crackFrontTangent *= -1;
        }
      }

      auto check_isnormal = [](double v) {
        MoFEMFunctionBeginHot;
        if (v != 0 && !std::isnormal(v)) {
          SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Not a number");
        }
        MoFEMFunctionReturnHot(0);
      };

      for (int rr = 0; rr != nb_rows; ++rr) {
        if (row_indices[rr] != -1) {
          // cerr << commonData.crackFrontTangent(3*rr+0) << " "
          // << commonData.crackFrontTangent(3*rr+1) << " "
          // << commonData.crackFrontTangent(3*rr+2) << endl;
          CHKERR check_isnormal(commonData.crackFrontTangent[3 * rr + 0]);
          CHKERR check_isnormal(commonData.crackFrontTangent[3 * rr + 1]);
          CHKERR check_isnormal(commonData.crackFrontTangent[3 * rr + 2]);
          cMat(rr, 3 * rr + 0) = commonData.crackFrontTangent[3 * rr + 0];
          cMat(rr, 3 * rr + 1) = commonData.crackFrontTangent[3 * rr + 1];
          cMat(rr, 3 * rr + 2) = commonData.crackFrontTangent[3 * rr + 2];
        }
      }

      if (!commonData.setMapV) {
        CHKERR MatSetValues(getFEMethod()->snes_B, nb_rows, &row_indices[0],
                            nb_cols, &col_indices[0], &cMat(0, 0), ADD_VALUES);
      } else {
        for (int rr = 0; rr != nb_rows; ++rr) {
          if (row_indices[rr] != -1) {
            if (commonData.mapV.find(row_indices[rr]) ==
                commonData.mapV.end()) {
              SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                      "Vector for given DOF in map not found");
            }
            for (int cc = 0; cc != nb_cols; ++cc) {
              if (col_indices[cc] != -1) {
                CHKERR check_isnormal(cMat(rr, cc));
              }
            }
            CHKERR VecSetValues(commonData.mapV[row_indices[rr]], nb_cols,
                                &col_indices[0], &cMat(rr, 0), ADD_VALUES);
          }
        }
      }

      MoFEMFunctionReturn(0);
    }
  };
};

} // namespace FractureMechanics
#endif //__CONSTANT_AREA_HPP__
