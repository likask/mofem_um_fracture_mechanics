
/** \file MWLS.cpp
 */

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#include <MoFEM.hpp>
using namespace MoFEM;
#include <cholesky.hpp>
#include <BasicFiniteElements.hpp>
#include <Mortar.hpp>
#include <Hooke.hpp>
#include <NeoHookean.hpp>
#include <CrackFrontElement.hpp>
#include <MWLS.hpp>
#include <BasicFiniteElements.hpp>
#include <GriffithForceElement.hpp>
#include <VolumeLengthQuality.hpp>
#include <ComplexConstArea.hpp>
#include <CrackPropagation.hpp>

namespace FractureMechanics {

MWLSApprox::MWLSApprox(MoFEM::Interface &m_field, Vec F_lambda,
                       boost::shared_ptr<DofEntity> arc_length_dof)
    : mField(m_field), mwlsMoab(mwlsCore), F_lambda(PETSC_NULL),
      arcLengthDof(arc_length_dof), nbBasePolynomials(1), dmFactor(1),
      maxElemsInDMFactor(2),
      useGlobalBaseAtMaterialReferenceConfiguration(PETSC_FALSE),
      useNodalData(PETSC_FALSE), mwlsAnalyticalInternalStressTest(PETSC_FALSE),
      approxPointCoords(3) {

  if (!LogManager::checkIfChannelExist("MWLSWorld")) {
    auto core_log = logging::core::get();

    core_log->add_sink(
        LogManager::createSink(LogManager::getStrmWorld(), "MWLSWorld"));
    core_log->add_sink(
        LogManager::createSink(LogManager::getStrmSync(), "MWLSSync"));
    core_log->add_sink(
        LogManager::createSink(LogManager::getStrmSelf(), "MWLSSelf"));

    LogManager::setLog("MWLSWorld");
    LogManager::setLog("MWLSSync");
    LogManager::setLog("MWLSSelf");

    MOFEM_LOG_TAG("MWLSWorld", "MWLS");
    MOFEM_LOG_TAG("MWLSSync", "MWLS");
    MOFEM_LOG_TAG("MWLSSelf", "MWLS");
  }

  MOFEM_LOG("MWLSWorld", Sev::noisy) << "MWLS created";

  rhoAtGaussPts = boost::make_shared<VectorDouble>();
  diffRhoAtGaussPts = boost::make_shared<MatrixDouble>();
  diffDiffRhoAtGaussPts = boost::make_shared<MatrixDouble>();
}

MoFEMErrorCode MWLSApprox::getMWLSOptions() {
  MoFEMFunctionBegin;
  ierr = PetscOptionsBegin(mField.get_comm(), "",
                           "Get options for moving least square approximation",
                           "none");
  CHKERRQ(ierr);
  CHKERR PetscOptionsScalar("-mwls_dm",
                            "edge length scaling for influence radius", "",
                            dmFactor, &dmFactor, PETSC_NULL);
  MOFEM_LOG_C("MWLSWorld", Sev::inform, "### Input parameter: -mwls_dm %6.4e",
              dmFactor);

  CHKERR PetscOptionsInt(
      "-mwls_number_of_base_functions",
      "1 = constant, 4 = linear, 10 = quadratic approximation", "",
      nbBasePolynomials, &nbBasePolynomials, PETSC_NULL);
  MOFEM_LOG_C("MWLSWorld", Sev::inform,
              "### Input parameter: -mwls_number_of_base_functions %d",
              nbBasePolynomials);

  CHKERR PetscOptionsInt("-mwls_max_elems_factor",
                         "Set max elements factor max_nb_elems = "
                         "maxElemsInDMFactor * nbBasePolynomials",
                         "", maxElemsInDMFactor, &maxElemsInDMFactor,
                         PETSC_NULL);
  MOFEM_LOG_C("MWLSWorld", Sev::inform,
              "### Input parameter: -mwls_max_elems_factor %d",
              maxElemsInDMFactor);

  CHKERR PetscOptionsBool(
      "-mwls_use_global_base_at_reference_configuration",
      "true local mwls base functions at reference configuration are used", "",
      useGlobalBaseAtMaterialReferenceConfiguration,
      &useGlobalBaseAtMaterialReferenceConfiguration, NULL);
  CHKERRQ(ierr);

  CHKERR PetscOptionsBool(
      "-mwls_use_nodal_data",
      "true nodal data values are used (without averaging to the midpoint)", "",
      useNodalData, &useNodalData, NULL);
  MOFEM_LOG_C("MWLSWorld", Sev::verbose,
              "### Input parameter: -mwls_use_nodal_data %d", useNodalData);

  CHKERR PetscOptionsBool(
      "-mwls_analytical_internal_stress",
      "use analytical strain field for internal stress mwls test", "",
      mwlsAnalyticalInternalStressTest, &mwlsAnalyticalInternalStressTest,
      NULL);
  MOFEM_LOG_C("MWLSWorld", Sev::verbose,
              "### Input parameter: -test_mwls_internal_stress_analytical %d",
              mwlsAnalyticalInternalStressTest);

  maxThreeDepth = 30;
  CHKERR PetscOptionsInt("-mwls_tree_depth", "maximal three depths", "",
                         maxThreeDepth, &maxThreeDepth, PETSC_NULL);
  MOFEM_LOG("MWLSWorld", Sev::verbose)
      << "Maximal MWLS three depths " << maxThreeDepth;
  splitsPerDir = 12;
  CHKERR PetscOptionsInt("-mwls_splits_per_dir", "splits per direction", "",
                         splitsPerDir, &splitsPerDir, PETSC_NULL);
  MOFEM_LOG("MWLSWorld", Sev::verbose)
      << "Splits per direction " << splitsPerDir;

  ierr = PetscOptionsEnd();
  CHKERRQ(ierr);
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MWLSApprox::loadMWLSMesh(std::string file_name) {
  MoFEMFunctionBegin;

  CHKERR getMWLSOptions();

  const char *option;
  option = "";
  CHKERR mwlsMoab.load_file(file_name.c_str(), 0, option);
  CHKERR mwlsMoab.get_entities_by_dimension(0, 3, levelTets);
  if (levelTets.empty()) {
    SETERRQ(mField.get_comm(), MOFEM_DATA_INCONSISTENCY,
            "No 3d element in MWLS mesh");
  }

  CHKERR mwlsMoab.get_adjacencies(levelTets, 1, true, levelEdges,
                                  moab::Interface::UNION);

  // Create HO node on tetrahedral
  if (!useNodalData) {
    EntityHandle meshset;
    CHKERR mwlsMoab.create_meshset(MESHSET_SET, meshset);
    CHKERR mwlsMoab.add_entities(meshset, levelTets);
    CHKERR mwlsMoab.convert_entities(meshset, false, false, true);
    CHKERR mwlsMoab.delete_entities(&meshset, 1);

    // Get HO nodes in TET center
    std::vector<double> edge_length;
    for (auto tet : levelTets) {
      int num_nodes;
      const EntityHandle *conn;
      CHKERR mwlsMoab.get_connectivity(tet, conn, num_nodes, false);
      EntityHandle ho_node;
      EntityType tit_type = mwlsMoab.type_from_handle(tet);
      CHKERR mwlsMoab.high_order_node(tet, conn, tit_type, ho_node);
      levelNodes.insert(ho_node);
    }

  } else {
    Range tets;
    CHKERR mwlsMoab.get_entities_by_dimension(0, 3, tets);
    CHKERR mwlsMoab.get_connectivity(tets, levelNodes, true);
  }

  // Store edge nodes coordinates in FTensor
  double edge_node_coords[6];
  FTensor::Tensor1<double *, 3> t_node_edge[2] = {
      FTensor::Tensor1<double *, 3>(edge_node_coords, &edge_node_coords[1],
                                    &edge_node_coords[2]),
      FTensor::Tensor1<double *, 3>(&edge_node_coords[3], &edge_node_coords[4],
                                    &edge_node_coords[5])};

  FTensor::Index<'i', 3> i;

  // Get edge lengths
  maxEdgeL = 0;
  for (auto edge : levelEdges) {
    int num_nodes;
    const EntityHandle *conn;
    CHKERR mwlsMoab.get_connectivity(edge, conn, num_nodes, true);
    CHKERR mwlsMoab.get_coords(conn, num_nodes, edge_node_coords);
    t_node_edge[0](i) -= t_node_edge[1](i);
    double l = sqrt(t_node_edge[0](i) * t_node_edge[0](i));
    maxEdgeL = (maxEdgeL < l) ? l : maxEdgeL;
  }

  // Create tree and find maximal edge length. Maximal edge length is used
  // to find neighbours nodes to material point.
  myTree = boost::make_shared<BVHTree>(&mwlsMoab);
  {
    std::ostringstream opts;
    opts << "MAX_DEPTH=" << maxThreeDepth
        //  << ";MAX_PER_LEAF=" << maxElemsInDMFactor * nbBasePolynomials + 1
         << ";SPLITS_PER_DIR=" << splitsPerDir;
    FileOptions tree_opts(opts.str().c_str());

    MOFEM_LOG_CHANNEL("MWLSWorld");
    MOFEM_LOG_TAG("MWLSWorld", "MWLS");
    BOOST_LOG_SCOPED_THREAD_ATTR("Timeline", attrs::timer());
    MOFEM_LOG("MWLSWorld", Sev::verbose) << "Build tree ... ";
    CHKERR myTree->build_tree(levelTets, &treeRoot, &tree_opts);
    MOFEM_LOG("MWLSWorld", Sev::verbose) << "done";
  }

  // moab::BoundBox box;
  // CHKERR myTree->get_bounding_box(box);
  // cout << box << endl;
  // myTree->print();

  auto map_elems_positions = [&]() {
    std::map<EntityHandle, std::array<double, 3>> elem_coords_map;
    Range tets;
    CHKERR mwlsMoab.get_entities_by_dimension(0, 3, tets, false);
    MatrixDouble elem_coords;
    for (auto p = tets.pair_begin(); p != tets.pair_end(); ++p) {
      EntityHandle *conn;
      int count, verts_per_e;
      Range slot(p->first, p->second);
      CHKERR mwlsMoab.connect_iterate(slot.begin(), slot.end(), conn,
                                      verts_per_e, count);
      if (count != (int)slot.size())
        THROW_MESSAGE("Should be one continuos sequence");
      elem_coords.resize(verts_per_e, 3, false);

      for (auto t = 0; t != count; ++t) {
        CHKERR mwlsMoab.get_coords(conn, verts_per_e,
                                   &*elem_coords.data().begin());
        auto get_center = [&]() {
          std::array<double, 3> c{0, 0, 0};
          for (auto d : {0, 1, 2})
            for (auto n = 0; n < verts_per_e; ++n)
              c[d] += elem_coords(n, d);
          for (auto d : {0, 1, 2})
            c[d] /= verts_per_e;
          return c;
        };
        elem_coords_map.emplace(std::make_pair(p->first + t, get_center()));
        conn += verts_per_e;
      }
    }
    return elem_coords_map;
  };

  elemCentreMap = map_elems_positions();

  // Set tag for MWLS stress when SSLV116 test is run
  if (mwlsAnalyticalInternalStressTest) {
    Tag th;
    std::array<double, 9> def;
    def.fill(0);
    CHKERR mwlsMoab.tag_get_handle("MED_SSLV116", 9, MB_TYPE_DOUBLE, th,
                                   MB_TAG_CREAT | MB_TAG_SPARSE, def.data());
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MWLSApprox::getValuesToNodes(Tag th) {
  MoFEMFunctionBegin;
  if (useNodalData)
    MoFEMFunctionReturnHot(0);

  CHKERR mwlsMoab.tag_get_handle("MID_NODE", 1, MB_TYPE_HANDLE, thMidNode,
                                 MB_TAG_CREAT | MB_TAG_DENSE);

  int length;
  CHKERR mwlsMoab.tag_get_length(th, length);

  VectorDouble vals(length);
  vals.clear();

  if (mwlsAnalyticalInternalStressTest == PETSC_FALSE) {

    string name;
    CHKERR mwlsMoab.tag_get_name(th, name);

    std::function<MoFEMErrorCode(EntityHandle)> get_data_to_node;
    if (name.compare("RHO") == 0) {
      get_data_to_node = [this, th, &vals](EntityHandle tet) {
        MoFEMFunctionBeginHot;
        Range nodes;
        double avg_val = 0;
        CHKERR mwlsMoab.get_connectivity(&tet, 1, nodes, true);
        for (auto &n : nodes) {
          double tag_data;
          CHKERR mwlsMoab.tag_get_data(th, &n, 1, &tag_data);
          avg_val += tag_data;
        }
        avg_val /= nodes.size();
        vals[0] = avg_val;
        MoFEMFunctionReturnHot(0);
      };
    } else {
      get_data_to_node = [this, th, &vals](EntityHandle tet) {
        MoFEMFunctionBeginHot;
        CHKERR mwlsMoab.tag_get_data(th, &tet, 1, &*vals.begin());
        MoFEMFunctionReturnHot(0);
      };
    }

    // clean tag on nodes
    CHKERR mwlsMoab.tag_clear_data(th, levelNodes, &*vals.begin());
    for (auto tet : levelTets) {

      CHKERR get_data_to_node(tet);
      int num_nodes;
      const EntityHandle *conn;
      CHKERR mwlsMoab.get_connectivity(tet, conn, num_nodes, false);
      EntityHandle ho_node;
      EntityType tet_type = mwlsMoab.type_from_handle(tet);
      CHKERR mwlsMoab.high_order_node(tet, conn, tet_type, ho_node);
      CHKERR mwlsMoab.tag_set_data(th, &ho_node, 1, &*vals.begin());
      CHKERR mwlsMoab.tag_set_data(thMidNode, &tet, 1, &ho_node);
    }

  } else {

    PetscBool add_analytical_internal_stress_operators = PETSC_FALSE;
    CHKERR PetscOptionsGetBool(PETSC_NULL, "",
                               "-add_analytical_internal_stress_operators",
                               &add_analytical_internal_stress_operators, NULL);

    CHKERR mwlsMoab.tag_clear_data(th, levelNodes, &*vals.begin());
    for (auto tet : levelTets) {
      int num_nodes;
      const EntityHandle *conn;
      CHKERR mwlsMoab.get_connectivity(tet, conn, num_nodes, false);
      EntityHandle ho_node;
      EntityType tet_type = mwlsMoab.type_from_handle(tet);
      CHKERR mwlsMoab.high_order_node(tet, conn, tet_type, ho_node);
      CHKERR mwlsMoab.tag_set_data(thMidNode, &tet, 1, &ho_node);

      if (add_analytical_internal_stress_operators == PETSC_FALSE) {
        VectorDouble coords(3);
        CHKERR mwlsMoab.get_coords(&ho_node, 1, &coords[0]);
        FTensor::Tensor1<FTensor::PackPtr<double *, 1>, 3> t_coords(
            &coords[0], &coords[1], &coords[2]);
        auto t_strain = CrackPropagation::analyticalStrainFunction(t_coords);

        constexpr double K = 166.666666666e9; // K = E / 3 / (1 - 2*nu),
                                              // E = 200 GPa, nu = 0.3

        // Only diagonal, vectorial notation
        vals[0] = 3 * K * t_strain(0, 0);
        vals[1] = 3 * K * t_strain(1, 1);
        vals[2] = 3 * K * t_strain(2, 2);
      }

      CHKERR mwlsMoab.tag_set_data(th, &ho_node, 1, &*vals.begin());
    }
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MWLSApprox::getInfluenceNodes(double material_coords[3]) {
  constexpr double eps_overlap = 0.01;
  const size_t max_nb_elems = maxElemsInDMFactor * nbBasePolynomials + 1;
  MoFEMFunctionBegin;

    using DistEntPair = std::pair<double, EntityHandle>;
    using DistEntMap = multi_index_container<
        DistEntPair,
        indexed_by<

            ordered_non_unique<member<DistEntPair, double, &DistEntPair::first>>

            >>;
    DistEntMap dist_map;

    // FIXME: That is potentisl source of problems, should be sparate
    // function for setting approximation base point, or simply should be set
    // outside of that function. Other functions will behave depending how
    // approxPointCoords is set. And here is somehow hidden from the view,
    // i.e. easy to overlook this. TODO: create a new function like:
    // MoFEMErrorCode setApproxBasePt(double material_coords[3]) ?

    for (int dd : {0, 1, 2})
      approxPointCoords[dd] = material_coords[dd];

    if (myTree) {
      shortenDm = maxEdgeL * dmFactor; // Influence radius

      auto find_leafs = [&](const auto dm) {
        leafsVec.clear();
        leafsDist.clear();
        CHKERR myTree->distance_search(material_coords, shortenDm, leafsVec,
                                       1.0e-10, 1.0e-6, &leafsDist);
        distLeafs.clear();
        distLeafs.reserve(leafsDist.size());
        for (size_t i = 0; i != leafsVec.size(); ++i) {
          distLeafs.emplace_back(leafsDist[i], leafsVec[i]);
        }
        std::sort(
            distLeafs.begin(), distLeafs.end(),
            [](const auto &a, const auto &b) { return a.first < b.first; });
        return distLeafs;
      };

      auto get_influence_nodes = [&](const auto &tets) {
        std::vector<EntityHandle> influence_nodes;
        if (!useNodalData) {
          influence_nodes.resize(tets.size());
          CHKERR mwlsMoab.tag_get_data(thMidNode, &*tets.begin(), tets.size(),
                                       &*influence_nodes.begin());
        } else {
          Range nodes;
          CHKERR mwlsMoab.get_connectivity(&*tets.begin(), tets.size(), nodes,
                                           true);
          influence_nodes.clear();
          influence_nodes.reserve(nodes.size());
          for (auto n : nodes)
            influence_nodes.emplace_back(n);
        }
        return influence_nodes;
      };

      auto to_range = [&](auto ents) {
        Range r;
        r.insert_list(ents.begin(), ents.end());
        return r;
      };

      auto save_entities = [&](const std::string name, Range ents) {
        MoFEMFunctionBeginHot;
        if (!ents.empty()) {
          EntityHandle meshset;
          CHKERR mwlsMoab.create_meshset(MESHSET_SET, meshset);
          CHKERR mwlsMoab.add_entities(meshset, ents);

          CHKERR mwlsMoab.write_file(name.c_str(), "VTK", "", &meshset, 1);
          CHKERR mwlsMoab.delete_entities(&meshset, 1);
        }
        MoFEMFunctionReturnHot(0);
      };

      auto leafs = find_leafs(shortenDm);

      treeTets.clear();
      std::vector<EntityHandle> leafs_tets;
      moab::BoundBox box;

      auto get_dm2_from_influence_points = [&]() {
        FTensor::Index<'i', 3> i;
        VectorDouble node_coors_vec(3 * influenceNodes.size());
        CHKERR mwlsMoab.get_coords(&*influenceNodes.begin(),
                                   influenceNodes.size(),
                                   &*node_coors_vec.begin());
        FTensor::Tensor1<double, 3> t_approx_point(
            material_coords[0], material_coords[1], material_coords[2]);
        FTensor::Tensor1<FTensor::PackPtr<double *, 3>, 3> t_node_coords(
            &node_coors_vec[0], &node_coors_vec[1], &node_coors_vec[2]);
        double dm2 = 0;
        for (auto node : influenceNodes) {
          t_node_coords(i) -= t_approx_point(i);
          const double r2 = t_node_coords(i) * t_node_coords(i);
          if (dm2 < r2)
            dm2 = r2;
          ++t_node_coords;
        }
        return dm2;
      };

      const double shorten_dm2 = shortenDm * shortenDm;
      double set_dm2 = shorten_dm2;
      auto min_max_dist_it = dist_map.get<0>().begin();
      FTensor::Index<'i', 3> i;

      for (auto l : leafs) {
        if (dist_map.size() < max_nb_elems ||
            ((l.first * l.first) <
             (set_dm2 + std::numeric_limits<double>::epsilon()))) {

          leafsTetsVecLeaf.clear();
          CHKERR mwlsMoab.get_entities_by_dimension(l.second, 3,
                                                    leafsTetsVecLeaf, false);

          leafsTetsCentre.resize(3 * leafsTetsVecLeaf.size());
          {
            FTensor::Tensor1<FTensor::PackPtr<double *, 1>, 3> t_c{
                &leafsTetsCentre[0], &leafsTetsCentre[leafsTetsVecLeaf.size()],
                &leafsTetsCentre[2 * leafsTetsVecLeaf.size()]};

            for (auto tet : leafsTetsVecLeaf) {
              const auto &c = elemCentreMap[tet];
              for (auto ii : {0, 1, 2}) {
                t_c(ii) = c[ii] - material_coords[ii];
              }
              ++t_c;
            }
          }

          {
            FTensor::Tensor1<FTensor::PackPtr<double *, 1>, 3> t_c{
                &leafsTetsCentre[0], &leafsTetsCentre[leafsTetsVecLeaf.size()],
                &leafsTetsCentre[2 * leafsTetsVecLeaf.size()]};

            for (auto tet : leafsTetsVecLeaf) {

              if (dist_map.size() > max_nb_elems)
                set_dm2 = std::min(min_max_dist_it->first, shorten_dm2);

              if (t_c(0) < set_dm2 && t_c(1) < set_dm2 && t_c(2) < set_dm2) {
                double dm2 = t_c(i) * t_c(i);
                if (dm2 < set_dm2) {
                  min_max_dist_it =
                      dist_map.get<0>().emplace(std::make_pair(dm2, tet)).first;
                  if (dist_map.size() > max_nb_elems) {
                    int nb = std::distance(dist_map.get<0>().begin(),
                                           min_max_dist_it);
                    for (; nb < max_nb_elems; ++nb)
                      ++min_max_dist_it;
                  }
                }
              }

              ++t_c;
            }
          }
        } else {
          break;
        }
      }

      treeTets.clear();
      treeTets.reserve(dist_map.size());
      const double dm2 = set_dm2 + std::numeric_limits<double>::epsilon();
      auto hi_it = dist_map.get<0>().upper_bound(dm2);
      for (auto it = dist_map.get<0>().begin(); it != hi_it; ++it)
        treeTets.emplace_back(it->second);

      if (treeTets.empty()) {
        for (auto &it : dist_map)
          MOFEM_LOG("MWLSWorld", Sev::error)
              << "dm map " << it.first << " " << it.second;

        MOFEM_LOG("MWLSWorld", Sev::error) << "leafs found " << leafs.size();
        MOFEM_LOG("MWLSWorld", Sev::error)
            << "dist map size " << dist_map.size();
        MOFEM_LOG("MWLSWorld", Sev::error) << "dm2 " << dm2;
        MOFEM_LOG("MWLSWorld", Sev::error) << "shorten dm is " << shortenDm;
        MOFEM_LOG_C("MWLSWorld", Sev::error, "point:  ( %g %g %g )",
                    approxPointCoords(0), approxPointCoords(1),
                    approxPointCoords(2));
        SETERRQ(mField.get_comm(), MOFEM_DATA_INCONSISTENCY, "No leafs found.");
      }

      influenceNodes = get_influence_nodes(treeTets);
      const auto dm2_from_influence_pts = get_dm2_from_influence_points();
      shortenDm = (1 + eps_overlap) * sqrt(dm2_from_influence_pts);

      // CHKERR save_entities("tree_tets.vtk", to_range(influenceNodes));
  } else {
    SETERRQ(mField.get_comm(), MOFEM_DATA_INCONSISTENCY, "kd-three not build");
  }
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MWLSApprox::calculateApproxCoeff(bool trim_influence_nodes,
                                                bool global_derivatives) {
  MoFEMFunctionBegin;

  auto to_range = [&](auto ents) {
    Range r;
    r.insert_list(ents.begin(), ents.end());
    return r;
  };

  auto save_entities = [&](const std::string name, Range ents) {
    MoFEMFunctionBeginHot;
    if (!ents.empty()) {
      EntityHandle meshset;
      CHKERR mwlsMoab.create_meshset(MESHSET_SET, meshset);
      CHKERR mwlsMoab.add_entities(meshset, ents);

      CHKERR mwlsMoab.write_file(name.c_str(), "VTK", "", &meshset, 1);
      CHKERR mwlsMoab.delete_entities(&meshset, 1);
    }
    MoFEMFunctionReturnHot(0);
  };

  // Determine points in influence volume
  auto trim_nodes = [&](const double dm) {
    MoFEMFunctionBeginHot;
    FTensor::Index<'i', 3> i;
    FTensor::Tensor1<double, 3> t_approx_point(
        approxPointCoords[0], approxPointCoords[1], approxPointCoords[2]);
    const double dm2 = shortenDm * shortenDm;
    VectorDouble node_coors_vec(3 * influenceNodes.size());
    CHKERR mwlsMoab.get_coords(&*influenceNodes.begin(), influenceNodes.size(),
                               &*node_coors_vec.begin());
    FTensor::Tensor1<FTensor::PackPtr<double *, 3>, 3> t_node_coords(
        &node_coors_vec[0], &node_coors_vec[1], &node_coors_vec[2]);
    decltype(influenceNodes) influence_nodes_tmp;
    influence_nodes_tmp.reserve(influenceNodes.size());
    for (auto node : influenceNodes) {
      t_node_coords(i) -= t_approx_point(i);
      const double r2 = t_node_coords(i) * t_node_coords(i);
      if (r2 < dm2)
        influence_nodes_tmp.emplace_back(node);
      ++t_node_coords;
    }
    influenceNodes.swap(influence_nodes_tmp);
    MoFEMFunctionReturnHot(0);
  };

  auto eval_AB = [this, global_derivatives](const double dm) {
    MoFEMFunctionBegin;
    FTensor::Index<'i', 3> i;

    A.resize(nbBasePolynomials, nbBasePolynomials, false);
    B.resize(nbBasePolynomials, influenceNodes.size(), false);
    A.clear();
    B.clear();
    for (int d = 0; d != 3; d++) {
      diffA[d].resize(nbBasePolynomials, nbBasePolynomials, false);
      diffB[d].resize(nbBasePolynomials, influenceNodes.size(), false);
      diffA[d].clear();
      diffB[d].clear();
    }

    FTensor::Tensor1<double, 3> t_approx_point(
        approxPointCoords[0], approxPointCoords[1], approxPointCoords[2]);

    VectorDouble nodes_coords(3 * influenceNodes.size());
    CHKERR mwlsMoab.get_coords(&*influenceNodes.begin(), influenceNodes.size(),
                               &*nodes_coords.begin());
    double *ptr = &*nodes_coords.begin();
    FTensor::Tensor1<FTensor::PackPtr<double *, 3>, 3> t_node_coords(
        ptr, ptr + 1, ptr + 2);

    double min_distance = -1;
    nearestInfluenceNode = 0;

    for (int ii = 0; ii != influenceNodes.size(); ++ii) {
      t_node_coords(i) -= t_approx_point(i);
      t_node_coords(i) /= shortenDm;
      const double r = sqrt(t_node_coords(i) * t_node_coords(i));

      // Get the influence node that has minimum distance from the node in the
      // projected mesh
      if (!ii || r < min_distance) {
        min_distance = r;
        nearestInfluenceNode = influenceNodes[ii];
      }

      FTensor::Tensor1<double, 3> t_diff_r;
      if (r > 0) {
        t_diff_r(i) = (1. / r) * t_node_coords(i) * (-1 / shortenDm);
      } else {
        t_diff_r(i) = 0;
      }
      // Weights
      const double w = evalWeight(r);
      calculateBase<FTensor::Tensor1<FTensor::PackPtr<double *, 3>, 3>>(
          t_node_coords);

      // iterate base functions
      // A_kj = sum_ii  w * b_k * b_j
      //
      // B_k = w * b_k
      //
      // diffA_kj = sum_ii diff_w * b_k * b_j + w * diff_b_k * b_j + w * b_k *
      // diff_b_j
      //
      // diffB =  diff_w * b_k + w * diff_bk
      for (int k = 0; k != nbBasePolynomials; k++) {
        const double wp = w * baseFun[k];
        for (int j = 0; j <= k; j++) {
          A(k, j) += wp * baseFun[j];
        }
        B(k, ii) = wp;
        if (global_derivatives) {
          const double diff_w_r = evalDiffWeight(r);
          const double diff_wp_r = diff_w_r * baseFun[k];
          for (int d = 0; d != 3; d++) {
            const double diff_wp_r_dx = diff_wp_r * t_diff_r(d);
            for (int j = 0; j <= k; j++) {
              diffA[d](k, j) += diff_wp_r_dx * baseFun[j];
            }
            diffB[d](k, ii) = diff_wp_r_dx;
          }
        }
      }
      ++t_node_coords;
    }
    if (nearestInfluenceNode == 0) {
      SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
              "Nearest node not found \n");
    }

    MoFEMFunctionReturn(0);
  };

  // Invert matrix A
  auto invert_A = [this]() {
    MoFEMFunctionBegin;

    auto solve = [&](const size_t nb_base_functions) {
      A.resize(nb_base_functions, nb_base_functions, true);
      L.resize(nb_base_functions, nb_base_functions, false);
      if (cholesky_decompose(A, L)) {
        return false;
      } else {

        MatrixDouble inv_a(nb_base_functions, nb_base_functions, false);
        for (int k = 0; k != nb_base_functions; k++) {
          ublas::matrix_column<MatrixDouble> mr(inv_a, k);
          mr(k) = 1;
          cholesky_solve(L, mr, ublas::lower());
        }

        if (nb_base_functions == nbBasePolynomials) {
          invA.swap(inv_a);
        } else {
          invA.resize(nbBasePolynomials, nbBasePolynomials, false);
          invA.clear();
          for (size_t i = 0; i != nb_base_functions; ++i)
            for (size_t j = 0; j != nb_base_functions; ++j)
              invA(i, j) = inv_a(i, j);
        }

        return true;
      }
    };

    auto throw_error = [&]() {
      MoFEMFunctionBegin;
      FTensor::Tensor1<double, 3> t_approx_point(
          approxPointCoords[0], approxPointCoords[1], approxPointCoords[2]);
      cerr << "Point: " << t_approx_point << endl;
      cerr << "Matrix A: " << A << endl;
      SETERRQ1(mField.get_comm(), MOFEM_OPERATION_UNSUCCESSFUL,
               "Failed to invert matrix - solution could be "
               "to increase dmFactor to bigger value, e.g. -mwls_dm 4, "
               "or reduce of number of base fuctions "
               "-mwls_number_of_base_functions 1. The number of nodes "
               "found in "
               "radius is %u",
               influenceNodes.size());
      MoFEMFunctionReturn(0);
    };

    auto solve_one = [&]() {
      MoFEMFunctionBegin;
      if (!solve(1))
        CHKERR throw_error();
      MoFEMFunctionReturn(0);
    };

    testA = A;
    if (!solve(nbBasePolynomials)) {
      if (nbBasePolynomials == 10) {
        if (!solve(4))
          CHKERR solve_one();
      } else if (nbBasePolynomials == 4) {
        CHKERR solve_one();
      } else {
        CHKERR throw_error();
      }
    }

    MoFEMFunctionReturn(0);
  };

  auto calculate_shape_functions_coefficients = [this]() {
    MoFEMFunctionBegin;
    // Calculate base functions (shape functions) coefficients
    invAB.resize(invA.size1(), B.size2(), false);
    noalias(invAB) = prod(invA, B);
    MoFEMFunctionReturn(0);
  };

  // CHKERR trim_nodes(shortenDm);
  // CHKERR save_entities("tree_tets_trim.vtk", to_range(influenceNodes));

  CHKERR eval_AB(shortenDm);
  CHKERR invert_A();
  CHKERR calculate_shape_functions_coefficients();

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MWLSApprox::evaluateApproxFun(double eval_point[3]) {
  MoFEMFunctionBegin;
  // Determine points in influence volume

  auto cal_base_functions_at_eval_point = [this, eval_point](const double dm) {
    MoFEMFunctionBegin;

    FTensor::Tensor1<double, 3> t_approx_point(
        approxPointCoords[0], approxPointCoords[1], approxPointCoords[2]);
    FTensor::Tensor1<double, 3> t_eval_point(eval_point[0], eval_point[1],
                                             eval_point[2]);

    FTensor::Index<'i', 3> i;
    FTensor::Tensor1<double, 3> t_delta;
    t_delta(i) = t_eval_point(i) - t_approx_point(i);
    t_delta(i) /= dm;

    CHKERR calculateBase(t_delta);
    MoFEMFunctionReturn(0);
  };

  auto calculate_shape_functions = [this]() {
    MoFEMFunctionBegin;
    // Calculate approximation functions at eval point
    approxFun.resize(influenceNodes.size(), false);
    noalias(approxFun) = prod(baseFun, invAB);
    MoFEMFunctionReturn(0);
  };

  CHKERR cal_base_functions_at_eval_point(shortenDm);
  CHKERR calculate_shape_functions();

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MWLSApprox::evaluateFirstDiffApproxFun(double eval_point[3],
                                                      bool global_derivatives) {
  MoFEMFunctionBegin;

  auto cal_diff_base_functions_at_eval_point = [this,
                                                eval_point](const double dm) {
    MoFEMFunctionBegin;

    FTensor::Tensor1<double, 3> t_approx_point(
        approxPointCoords[0], approxPointCoords[1], approxPointCoords[2]);
    FTensor::Tensor1<double, 3> t_eval_point(eval_point[0], eval_point[1],
                                             eval_point[2]);

    FTensor::Index<'i', 3> i;
    FTensor::Tensor1<double, 3> t_delta;
    t_delta(i) = t_eval_point(i) - t_approx_point(i);
    t_delta(i) /= dm;

    CHKERR calculateDiffBase(t_delta, dm);
    MoFEMFunctionReturn(0);
  };

  auto calculate_global_shape_functions_derivatives = [this]() {
    MoFEMFunctionBegin;

    // Calculate derivatives of base functions
    VectorDouble tmp;
    tmp.resize(nbBasePolynomials, false);
    baseFunInvA.resize(invA.size2(), false);
    noalias(baseFunInvA) = prod(baseFun, invA);

    for (int d = 0; d != 3; d++) {
      diffInvA[d].resize(nbBasePolynomials, nbBasePolynomials, false);
      diffApproxFun.resize(3, influenceNodes.size(), false);
    }

    for (int d = 0; d != 3; d++) {
      MatrixDouble a = prod(diffA[d], invA);
      noalias(diffInvA[d]) = -prod(invA, a);
    }

    // Calculate line derivatives for each coordinate direction
    for (int d = 0; d != 3; ++d) {

      ublas::matrix_row<MatrixDouble> mr(diffApproxFun, d);

      // b,x * A^-1 B
      noalias(mr) = prod(diffBaseFun[d], invAB);

      // b * (A^-1 A,x A^-1) * B
      noalias(tmp) = prod(baseFun, diffInvA[d]);
      mr += prod(tmp, B);

      // b * A^-1 * B,x
      mr += prod(baseFunInvA, diffB[d]);
    }

    MoFEMFunctionReturn(0);
  };

  auto calculate_local_shape_functions_derivatives = [this]() {
    MoFEMFunctionBegin;

    // Calculate derivatives of base functions
    diffApproxFun.resize(3, influenceNodes.size(), false);
    // Calculate line derivatives for each coordinate direction
    for (int d = 0; d != 3; ++d) {
      ublas::matrix_row<MatrixDouble> mr(diffApproxFun, d);
      // b,x * A^-1 B
      noalias(mr) = prod(diffBaseFun[d], invAB);
    }

    MoFEMFunctionReturn(0);
  };

  CHKERR cal_diff_base_functions_at_eval_point(shortenDm);
  if (global_derivatives)
    CHKERR calculate_global_shape_functions_derivatives();
  else
    CHKERR calculate_local_shape_functions_derivatives();

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode
MWLSApprox::evaluateSecondDiffApproxFun(double eval_point[3],
                                        bool global_derivatives) {
  MoFEMFunctionBegin;

  auto cal_diff_diff_base_functions_at_eval_point =
      [this, eval_point](const double dm) {
        MoFEMFunctionBegin;

        FTensor::Tensor1<double, 3> t_approx_point(
            approxPointCoords[0], approxPointCoords[1], approxPointCoords[2]);
        FTensor::Tensor1<double, 3> t_eval_point(eval_point[0], eval_point[1],
                                                 eval_point[2]);

        FTensor::Index<'i', 3> i;
        FTensor::Tensor1<double, 3> t_delta;
        t_delta(i) = t_eval_point(i) - t_approx_point(i);
        t_delta(i) /= dm;

        CHKERR calculateDiffDiffBase(t_delta, dm);
        MoFEMFunctionReturn(0);
      };

  auto calculate_global_shape_functions_second_derivatives = [this]() {
    MoFEMFunctionBegin;
    // to be implemented
    MoFEMFunctionReturn(0);
  };

  auto calculate_local_shape_functions_second_derivatives = [this]() {
    MoFEMFunctionBegin;

    // Calculate derivatives of base functions
    // for (int d = 0; d != 3; d++) {
    diffDiffApproxFun.resize(9, influenceNodes.size(), false);
    // }
    // Calculate line derivatives for each coordinate direction
    for (int n = 0; n != 3; ++n) {
      for (int d = 0; d != 3; ++d) {
        const int indx = d + 3 * n;
        ublas::matrix_row<MatrixDouble> mr(diffDiffApproxFun, indx);
        // b,xy * A^-1 B
        noalias(mr) = prod(diffDiffBaseFun[indx], invAB);
      }
    }
    MoFEMFunctionReturn(0);
  };

  CHKERR cal_diff_diff_base_functions_at_eval_point(shortenDm);
  if (global_derivatives) {
    SETERRQ(PETSC_COMM_SELF, MOFEM_NOT_IMPLEMENTED, "Not implemented yet");
    // CHKERR calculate_global_shape_functions_second_derivatives();
  } else
    CHKERR calculate_local_shape_functions_second_derivatives();

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MWLSApprox::getTagData(Tag th) {
  MoFEMFunctionBegin;
  int length;
  CHKERR mwlsMoab.tag_get_length(th, length);
  influenceNodesData.resize(influenceNodes.size(), length, false);
  CHKERR mwlsMoab.tag_get_data(th, &*influenceNodes.begin(),
                               influenceNodes.size(),
                               &influenceNodesData(0, 0));
  outData.resize(length, false);
  outDiffData.resize(3, length, false);
  outDiffDiffData.resize(9, length, false);
  MoFEMFunctionReturn(0);
}

const MWLSApprox::VecVals &MWLSApprox::getDataApprox() {
  noalias(outData) = prod(approxFun, influenceNodesData);
  return outData;
}

const MWLSApprox::MatDiffVals &MWLSApprox::getDiffDataApprox() {
  noalias(outDiffData) = prod(diffApproxFun, influenceNodesData);
  return outDiffData;
}

const MWLSApprox::MatDiffDiffVals &MWLSApprox::getDiffDiffDataApprox() {
  noalias(outDiffDiffData) = prod(diffDiffApproxFun, influenceNodesData);
  return outDiffDiffData;
}

MoFEMErrorCode MWLSApprox::getErrorAtNode(
    Tag th, double &total_stress_at_node, double &total_stress_error_at_node,
    double &hydro_static_error_at_node, double &deviatoric_error_at_node,
    double &total_energy, double &total_energy_error,
    const double young_modulus, const double poisson_ratio) {
  MoFEMFunctionBegin;

  int length;
  CHKERR mwlsMoab.tag_get_length(th, length);

  FTensor::Index<'i', 3> i;
  FTensor::Index<'j', 3> j;
  FTensor::Index<'k', 3> k;
  FTensor::Index<'l', 3> l;

  auto get_compliance_matrix = [&]() {
    FTensor::Ddg<double, 3, 3> t_C;
    t_C(i, j, k, l) = 0;

    const double c = poisson_ratio / young_modulus;
    t_C(0, 0, 0, 0) = 1. / young_modulus;
    t_C(0, 0, 1, 1) = -c;
    t_C(0, 0, 2, 2) = -c;

    t_C(1, 1, 0, 0) = -c;
    t_C(1, 1, 1, 1) = 1. / young_modulus;
    t_C(1, 1, 2, 2) = -c;

    t_C(2, 2, 0, 0) = -c;
    t_C(2, 2, 1, 1) = -c;
    t_C(2, 2, 2, 2) = 1. / young_modulus;

    const double d = (1 + poisson_ratio) / young_modulus;
    t_C(0, 1, 0, 1) = d;
    t_C(0, 2, 0, 2) = d;
    t_C(1, 2, 1, 2) = d;

    return t_C;
  };

  auto t_C = get_compliance_matrix();

  if (length != outData.size())
    SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
            "Tag length is not consistent with outData size. \nMost probably "
            "getTagData has not been invoked yet!\n");

  VectorDouble val_at_nearest_influence_node;
  val_at_nearest_influence_node.resize(length, false);
  val_at_nearest_influence_node.clear();
  CHKERR mwlsMoab.tag_get_data(th, &nearestInfluenceNode, 1,
                               &*val_at_nearest_influence_node.begin());

  // SIXX 0 (00) SIYY 1 (11) SIZZ 2 (22) SIXY 3 (01) SIXZ 4 (02) SIYZ 5 (12)
  // T* d00, T* d01, T* d02, T* d11, T* d12, T* d22
  auto get_symm_tensor = [](auto &m) {
    FTensor::Tensor2_symmetric<double *, 3> t(

        &m(0), &m(3), &m(4),

        &m(1), &m(5),

        &m(2)

    );
    return t;
  };

  auto t_total_stress_error = FTensor::Tensor2_symmetric<double, 3>();
  auto t_stress_at_nearset_node =
      get_symm_tensor(val_at_nearest_influence_node);
  auto t_mwls_stress = get_symm_tensor(outData);

  auto get_energy_norm = [&](auto &t_s) {
    return 0.5 * t_s(i, j) * (t_C(i, j, k, l) * t_s(k, l));
  };
  auto get_stress_nrm2 = [i, j](auto &t_s) { return t_s(i, j) * t_s(i, j); };

  total_stress_at_node = get_stress_nrm2(t_stress_at_nearset_node);
  total_energy = get_energy_norm(t_stress_at_nearset_node);

  t_total_stress_error(i, j) =
      t_mwls_stress(i, j) - t_stress_at_nearset_node(i, j);
  total_stress_error_at_node = get_stress_nrm2(t_total_stress_error);
  total_energy_error = get_energy_norm(t_total_stress_error);

  constexpr auto t_kd = FTensor::Kronecker_Delta_symmetric<int>();
  // calculate error of the hydro static part of the stress
  double hydro_static_difference =
      (t_kd(i, j) * t_total_stress_error(i, j)) / 3.;
  hydro_static_error_at_node =
      hydro_static_difference * hydro_static_difference;

  // calculate error of the deviator part of the stress
  auto t_dev_error = FTensor::Tensor2_symmetric<double, 3>();
  t_dev_error(i, j) =
      t_total_stress_error(i, j) - hydro_static_difference * t_kd(i, j);
  deviatoric_error_at_node = get_stress_nrm2(t_dev_error);

  MoFEMFunctionReturn(0);
}

template <class T>
MoFEMErrorCode
MWLSApprox::OpMWLSBase<T>::doWork(int side, EntityType type,
                                  DataForcesAndSourcesCore::EntData &data) {
  MoFEMFunctionBegin;
  if (type != MBVERTEX) {
    MoFEMFunctionReturnHot(0);
  }

  if (std::is_same<T, VolumeElementForcesAndSourcesCore>::value)
    if (mwlsApprox->tetsInBlock.find(this->getFEEntityHandle()) ==
        mwlsApprox->tetsInBlock.end()) {
      MoFEMFunctionReturnHot(0);
    }

  if (std::is_same<T, FaceElementForcesAndSourcesCore>::value)
    if (mwlsApprox->trisInBlock.find(this->getFEEntityHandle()) ==
        mwlsApprox->trisInBlock.end()) {
      MoFEMFunctionReturnHot(0);
    }

  const int nb_gauss_pts = data.getN().size1();

  FTensor::Index<'i', 3> i;
  FTensor::Index<'j', 3> j;

  mwlsApprox->approxBasePoint.resize(3, nb_gauss_pts, false);
  mwlsApprox->approxBasePoint.clear();
  mwlsApprox->singularInitialDisplacement.resize(3, nb_gauss_pts, false);
  mwlsApprox->singularInitialDisplacement.clear();
  mwlsApprox->singularCurrentDisplacement.resize(3, nb_gauss_pts, false);
  mwlsApprox->singularCurrentDisplacement.clear();

  if (auto fe_ptr = feSingularPtr.lock()) {

    if (fe_ptr && fe_ptr->singularElement) {

      if (!matPosAtPtsPtr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                "Matrix for material positions not acclocated");

      auto t_material_positions = getFTensor1FromMat<3>(*matPosAtPtsPtr);
      mwlsApprox->mwlsMaterialPositions.resize(matPosAtPtsPtr->size1(),
                                               matPosAtPtsPtr->size2(), false);
      auto t_mwls_material_positions =
          getFTensor1FromMat<3>(mwlsApprox->mwlsMaterialPositions);

      FTensor::Tensor1<FTensor::PackPtr<double *, 3>, 3>
          t_singular_displacement(&fe_ptr->singularDisp(0, 0),
                                  &fe_ptr->singularDisp(0, 1),
                                  &fe_ptr->singularDisp(0, 2));

      auto t_inital_singular_displacement =
          getFTensor1FromMat<3>(mwlsApprox->singularInitialDisplacement);
      auto t_current_singular_displacement =
          getFTensor1FromMat<3>(mwlsApprox->singularCurrentDisplacement);
      if (!matGradPosAtPtsPtr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                "Matrix for gradient of positions not allocated");
      auto t_H = getFTensor2FromMat<3, 3>(*matGradPosAtPtsPtr);
      for (int gg = 0; gg != nb_gauss_pts; gg++) {

        t_inital_singular_displacement(i) = t_singular_displacement(i);
        t_current_singular_displacement(i) =
            t_H(i, j) * t_singular_displacement(j);
        t_mwls_material_positions(i) =
            t_material_positions(i) + t_current_singular_displacement(i);

        ++t_mwls_material_positions;
        ++t_material_positions;
        ++t_H;
        ++t_singular_displacement;
        ++t_inital_singular_displacement;
        ++t_current_singular_displacement;
      }
    } else {
      mwlsApprox->mwlsMaterialPositions = *matPosAtPtsPtr;
    }
  }

  const bool use_global_base =
      mwlsApprox->getUseGlobalBaseAtMaterialReferenceConfiguration();

  if (use_global_base) {
    auto t_mwls_material_positions =
        getFTensor1FromMat<3>(mwlsApprox->mwlsMaterialPositions);
    auto t_approx_base_point =
        getFTensor1FromMat<3>(mwlsApprox->approxBasePoint);
    for (int gg = 0; gg != nb_gauss_pts; gg++) {
      t_approx_base_point(i) = t_mwls_material_positions(i);
      ++t_approx_base_point;
      ++t_mwls_material_positions;
    }
  } else {

    auto fe_ptr = feSingularPtr.lock();
    if (fe_ptr && fe_ptr->singularElement) {
      auto t_inital_singular_displacement =
          getFTensor1FromMat<3>(mwlsApprox->singularInitialDisplacement);
      auto t_approx_base_point =
          getFTensor1FromMat<3>(mwlsApprox->approxBasePoint);
      auto t_coords_of_gauss_point = this->getFTensor1CoordsAtGaussPts();
      for (int gg = 0; gg != nb_gauss_pts; gg++) {
        t_approx_base_point(i) =
            t_coords_of_gauss_point(i) + t_inital_singular_displacement(i);
        ++t_approx_base_point;
        ++t_coords_of_gauss_point;
        ++t_inital_singular_displacement;
      }
    } else {
      auto t_approx_base_point =
          getFTensor1FromMat<3>(mwlsApprox->approxBasePoint);
      auto t_coords_of_gauss_point = this->getFTensor1CoordsAtGaussPts();
      for (int gg = 0; gg != nb_gauss_pts; gg++) {
        t_approx_base_point(i) = t_coords_of_gauss_point(i);
        ++t_approx_base_point;
        ++t_coords_of_gauss_point;
      }
    }
  }

  CHKERR doMWLSWork(side, type, data);

  MoFEMFunctionReturn(0);
}

template <class T>
MoFEMErrorCode
MWLSApprox::OpMWLSCalculateBaseCoeffcientsAtGaussPtsTmpl<T>::doMWLSWork(
    int side, EntityType type, DataForcesAndSourcesCore::EntData &data) {
  MoFEMFunctionBegin;

  FTensor::Index<'i', 3> i;
  const int nb_gauss_pts = data.getN().size1();
  auto &mwls = this->mwlsApprox;

  if (mwls->invABMap.find(this->getFEEntityHandle()) == mwls->invABMap.end() ||
      mwls->influenceNodesMap.find(this->getFEEntityHandle()) ==
          mwls->influenceNodesMap.end()) {
          
    // MOFEM_LOG("MWLSSelf", Sev::noisy) << "Calculate coeffs" << endl;

    const bool use_global_base =
        mwls->getUseGlobalBaseAtMaterialReferenceConfiguration();
    if (use_global_base)
      SETERRQ(PETSC_COMM_SELF, MOFEM_IMPOSSIBLE_CASE,
              "Coefficients can be precalculated only for local base in "
              "reference configuration");

    auto &inv_AB_map = mwls->invABMap[this->getFEEntityHandle()];
    inv_AB_map.resize(nb_gauss_pts);
    auto &influence_nodes_map =
        mwls->influenceNodesMap[this->getFEEntityHandle()];
    influence_nodes_map.resize(nb_gauss_pts);
    auto &dm_nodes_map = mwls->dmNodesMap[this->getFEEntityHandle()];
    dm_nodes_map.resize(nb_gauss_pts);

    auto t_approx_base_point = getFTensor1FromMat<3>(mwls->approxBasePoint);
    for (int gg = 0; gg != nb_gauss_pts; ++gg) {

      FTensor::Tensor1<double, 3> t_approx_base_pos;
      t_approx_base_pos(i) = t_approx_base_point(i);

      CHKERR mwls->getInfluenceNodes(&t_approx_base_pos(0));
      CHKERR mwls->calculateApproxCoeff(true, false);

      influence_nodes_map[gg] = mwls->influenceNodes;
      MatrixDouble &invAB = mwls->invAB;
      inv_AB_map[gg].resize(invAB.size1(), invAB.size2(), false);
      noalias(inv_AB_map[gg]) = invAB;
      dm_nodes_map[gg] = mwls->shortenDm;

      ++t_approx_base_point;
    }
  }

  MoFEMFunctionReturn(0);
}

template struct MWLSApprox::OpMWLSCalculateBaseCoeffcientsAtGaussPtsTmpl<
    VolumeElementForcesAndSourcesCore>;
template struct MWLSApprox::OpMWLSCalculateBaseCoeffcientsAtGaussPtsTmpl<
    FaceElementForcesAndSourcesCore>;

MoFEMErrorCode MWLSApprox::OpMWLSRhoAtGaussUsingPrecalulatedCoeffs::doMWLSWork(
    int side, EntityType type, DataForcesAndSourcesCore::EntData &data) {
  MoFEMFunctionBegin;

  if (testing)
    cerr << "Element " << getFEEntityHandle() << endl;
  const bool use_global_base =
      mwlsApprox->getUseGlobalBaseAtMaterialReferenceConfiguration();
  if (use_global_base)
    SETERRQ(PETSC_COMM_SELF, MOFEM_IMPOSSIBLE_CASE,
            "Coefficients can be precalculated only for local base in "
            "reference configuration");

  FTensor::Index<'i', 3> i;

  Tag th;
  CHKERR mwlsApprox->mwlsMoab.tag_get_handle(rhoTagName.c_str(), th);

  const int nb_gauss_pts = data.getN().size1();
  auto &inv_AB_map = mwlsApprox->invABMap.at(getFEEntityHandle());
  auto &influence_nodes_map =
      mwlsApprox->influenceNodesMap.at(getFEEntityHandle());
  auto &dm_nodes_map = mwlsApprox->dmNodesMap.at(this->getFEEntityHandle());

  VectorDouble &rho = *mwlsApprox->rhoAtGaussPts;
  rho.resize(nb_gauss_pts, false);
  MatrixDouble &diff_rho = *mwlsApprox->diffRhoAtGaussPts;
  diff_rho.resize(3, nb_gauss_pts, false);
  MatrixDouble &diff_diff_rho = *mwlsApprox->diffDiffRhoAtGaussPts;
  if (calculate2ndDerivative)
    diff_diff_rho.resize(9, nb_gauss_pts, false);

  auto t_mwls_material_positions =
      getFTensor1FromMat<3>(mwlsApprox->mwlsMaterialPositions);
  auto t_approx_base_point = getFTensor1FromMat<3>(mwlsApprox->approxBasePoint);

  for (int gg = 0; gg != nb_gauss_pts; ++gg) {

    FTensor::Tensor1<double, 3> t_mat_pos;
    t_mat_pos(i) = t_mwls_material_positions(i);

    if (testing) {
      cerr << "material_positions " << t_mwls_material_positions << endl;
    }

    for (int dd : {0, 1, 2})
      mwlsApprox->approxPointCoords[dd] = t_approx_base_point(dd);

    mwlsApprox->influenceNodes = influence_nodes_map[gg];
    mwlsApprox->invAB = inv_AB_map[gg];
    mwlsApprox->shortenDm = dm_nodes_map[gg];

    CHKERR mwlsApprox->evaluateApproxFun(&t_mat_pos(0));
    CHKERR mwlsApprox->evaluateFirstDiffApproxFun(&t_mat_pos(0),
                                                  use_global_base);
    if (calculate2ndDerivative)
      CHKERR mwlsApprox->evaluateSecondDiffApproxFun(&t_mat_pos(0),
                                                     use_global_base);

    CHKERR mwlsApprox->getTagData(th);
    const auto &vals = mwlsApprox->getDataApprox();

    rho(gg) = vals[0];
    if (calculateDerivative) {
      const auto &diff_vals = mwlsApprox->getDiffDataApprox();
      for (int ii = 0; ii != 3; ++ii) {
        diff_rho(ii, gg) = diff_vals(ii, 0);
      }
    }

    if (calculate2ndDerivative) {
      const auto &diff_diff_vals = mwlsApprox->getDiffDiffDataApprox();
      for (int ii = 0; ii != 3; ++ii) {
        for (int jj = 0; jj != 3; ++jj)
          diff_diff_rho(jj * 3 + ii, gg) = diff_diff_vals(jj * 3 + ii, 0);
      }
    }

    ++t_mwls_material_positions;
    ++t_approx_base_point;
  }

  if (testing) {
    cerr << "rho " << rho << endl;
    cerr << endl;
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MWLSApprox::OpMWLSRhoAtGaussPts::doMWLSWork(
    int side, EntityType type, DataForcesAndSourcesCore::EntData &data) {
  MoFEMFunctionBegin;
  if (testing)
    cerr << "Element " << getFEEntityHandle() << endl;

  FTensor::Index<'i', 3> i;

  const int nb_gauss_pts = data.getN().size1();
  const bool use_global_base =
      mwlsApprox->getUseGlobalBaseAtMaterialReferenceConfiguration();
  Tag th;
  CHKERR mwlsApprox->mwlsMoab.tag_get_handle(rhoTagName.c_str(), th);

  VectorDouble &rho = *mwlsApprox->rhoAtGaussPts;
  rho.resize(nb_gauss_pts, false);
  MatrixDouble &diff_rho = *mwlsApprox->diffRhoAtGaussPts;
  diff_rho.resize(3, nb_gauss_pts, false);
  MatrixDouble &diff_diff_rho = *mwlsApprox->diffDiffRhoAtGaussPts;
  if (calculate2ndDerivative)
    diff_diff_rho.resize(9, nb_gauss_pts, false);

  auto t_mwls_material_positions =
      getFTensor1FromMat<3>(mwlsApprox->mwlsMaterialPositions);
  auto t_approx_base_point = getFTensor1FromMat<3>(mwlsApprox->approxBasePoint);
  for (int gg = 0; gg != nb_gauss_pts; ++gg) {

    FTensor::Tensor1<double, 3> t_mat_pos;
    t_mat_pos(i) = t_mwls_material_positions(i);
    FTensor::Tensor1<double, 3> t_approx_base_pos;
    t_approx_base_pos(i) = t_approx_base_point(i);

    if (testing) {
      cerr << "material_positions " << t_mwls_material_positions << endl;
      cerr << "approx_base_point " << t_approx_base_point << endl;
    }
    CHKERR mwlsApprox->getInfluenceNodes(&t_approx_base_pos(0));
    CHKERR mwlsApprox->calculateApproxCoeff(true, use_global_base);
    CHKERR mwlsApprox->evaluateApproxFun(&t_mat_pos(0));
    CHKERR mwlsApprox->evaluateFirstDiffApproxFun(&t_mat_pos(0),
                                                  use_global_base);
    if (calculate2ndDerivative)
      CHKERR mwlsApprox->evaluateSecondDiffApproxFun(&t_mat_pos(0),
                                                     use_global_base);
    CHKERR mwlsApprox->getTagData(th);
    const auto &vals = mwlsApprox->getDataApprox();

    rho(gg) = vals[0];
    if (calculateDerivative) {
      const auto &diff_vals = mwlsApprox->getDiffDataApprox();
      for (int ii = 0; ii != 3; ++ii) {
        diff_rho(ii, gg) = diff_vals(ii, 0);
      }
    }

    if (calculate2ndDerivative) {
      const auto &diff_diff_vals = mwlsApprox->getDiffDiffDataApprox();
      for (int ii = 0; ii != 3; ++ii) {
        for (int jj = 0; jj != 3; ++jj)
          diff_diff_rho(ii + 3 * jj, gg) = diff_diff_vals(jj, ii);
      }
    }

    ++t_mwls_material_positions;
    ++t_approx_base_point;
  }

  if (testing) {
    cerr << "rho " << rho << endl;
    cerr << endl;
  }

  MoFEMFunctionReturn(0);
}

PetscErrorCode MWLSApprox::OpMWLSRhoPostProcess::doWork(
    int side, EntityType type, DataForcesAndSourcesCore::EntData &data) {
  MoFEMFunctionBegin;
  if (type != MBVERTEX) {
    MoFEMFunctionReturnHot(0);
  }
  auto &mwls_approx = *mwlsApproxPtr;
  if (mwls_approx.tetsInBlock.find(getFEEntityHandle()) ==
      mwls_approx.tetsInBlock.end()) {
    MoFEMFunctionReturnHot(0);
  }

  auto &post_proc_mesh = *postProcMeshPtr;
  const auto &map_gauss_pts = *mapGaussPtsPtr;
  const auto &rho_at_gauss = *mwls_approx.rhoAtGaussPts;
  const auto &rho_diff_at_gauss = *mwls_approx.diffRhoAtGaussPts;

  double rho_tag = 0;
  VectorDouble diff_rho_tag(3);
  diff_rho_tag.clear();
  Tag tag_rho;
  Tag tag_diff_rho;

  CHKERR post_proc_mesh.tag_get_handle("RHO_APPROX", 1, MB_TYPE_DOUBLE, tag_rho,
                                       MB_TAG_CREAT | MB_TAG_SPARSE, &rho_tag);
  CHKERR post_proc_mesh.tag_get_handle(
      "RHO_DIFF_APPROX", 3, MB_TYPE_DOUBLE, tag_diff_rho,
      MB_TAG_CREAT | MB_TAG_SPARSE, &*diff_rho_tag.begin());

  const int nb_gauss_pts = data.getN().size1();
  // for elastic element
  // commonData.fieldMap["RHO"].resize(nb_gauss_pts); //delete this

  for (int gg = 0; gg != nb_gauss_pts; gg++) {
    const EntityHandle post_proc_node = map_gauss_pts[gg];
    rho_tag = rho_at_gauss(gg);
    CHKERR post_proc_mesh.tag_set_data(tag_rho, &post_proc_node, 1, &rho_tag);
    for (int ii : {0, 1, 2})
      diff_rho_tag[ii] = rho_diff_at_gauss(ii, gg);
    CHKERR post_proc_mesh.tag_set_data(tag_diff_rho, &post_proc_node, 1,
                                       &*diff_rho_tag.begin());
  }
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode
MWLSApprox::OpMWLSStressAtGaussUsingPrecalulatedCoeffs::doMWLSWork(
    int side, EntityType type, DataForcesAndSourcesCore::EntData &data) {
  MoFEMFunctionBegin;

  if (testing)
    cerr << "Element " << getFEEntityHandle() << endl;

  const bool use_global_base =
      mwlsApprox->getUseGlobalBaseAtMaterialReferenceConfiguration();
  if (use_global_base)
    SETERRQ(PETSC_COMM_SELF, MOFEM_IMPOSSIBLE_CASE,
            "Coefficients can be precalculated only for local base in "
            "reference configuration");

  FTensor::Index<'i', 3> i;

  Tag th;
  CHKERR mwlsApprox->mwlsMoab.tag_get_handle(stressTagName.c_str(), th);

  const int nb_gauss_pts = data.getN().size1();
  auto &inv_AB_map = mwlsApprox->invABMap.at(getFEEntityHandle());
  auto &influence_nodes_map =
      mwlsApprox->influenceNodesMap.at(getFEEntityHandle());
  auto &dm_nodes_map = mwlsApprox->dmNodesMap.at(this->getFEEntityHandle());

  MatrixDouble &stress = mwlsApprox->stressAtGaussPts;
  stress.resize(6, nb_gauss_pts, false);
  MatrixDouble &diff_stress = mwlsApprox->diffStressAtGaussPts;
  diff_stress.resize(18, nb_gauss_pts, false);

  auto t_mwls_material_positions =
      getFTensor1FromMat<3>(mwlsApprox->mwlsMaterialPositions);
  auto t_approx_base_point = getFTensor1FromMat<3>(mwlsApprox->approxBasePoint);

  for (int gg = 0; gg != nb_gauss_pts; ++gg) {

    FTensor::Tensor1<double, 3> t_mat_pos;
    t_mat_pos(i) = t_mwls_material_positions(i);

    if (testing) {
      cerr << "material_positions " << t_mwls_material_positions << endl;
    }

    for (int dd : {0, 1, 2})
      mwlsApprox->approxPointCoords[dd] = t_approx_base_point(dd);

    mwlsApprox->influenceNodes = influence_nodes_map[gg];
    mwlsApprox->invAB = inv_AB_map[gg];
    mwlsApprox->shortenDm = dm_nodes_map[gg];

    CHKERR mwlsApprox->evaluateApproxFun(&t_mat_pos(0));
    CHKERR mwlsApprox->evaluateFirstDiffApproxFun(&t_mat_pos(0),
                                                  use_global_base);

    CHKERR mwlsApprox->getTagData(th);
    const auto &vals = mwlsApprox->getDataApprox();

    for (int ii = 0; ii != 6; ++ii)
      stress(ii, gg) = vals[ii];
    if (calculateDerivative) {
      const auto &diff_vals = mwlsApprox->getDiffDataApprox();
      for (int ii = 0; ii != 6; ++ii) {
        for (int jj = 0; jj != 3; ++jj)
          diff_stress(jj * 6 + ii, gg) = diff_vals(jj, ii);
      }
    }
    ++t_mwls_material_positions;
    ++t_approx_base_point;
  }

  if (testing) {
    cerr << "stress " << stress << endl;
    cerr << endl;
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MWLSApprox::OpMWLSStressAtGaussPts::doMWLSWork(
    int side, EntityType type, DataForcesAndSourcesCore::EntData &data) {
  MoFEMFunctionBegin;

  if (testing)
    cerr << "Element " << getFEEntityHandle() << endl;

  FTensor::Index<'i', 3> i;

  const int nb_gauss_pts = data.getN().size1();
  const bool use_global_base =
      mwlsApprox->getUseGlobalBaseAtMaterialReferenceConfiguration();
  Tag th;
  CHKERR mwlsApprox->mwlsMoab.tag_get_handle(stressTagName.c_str(), th);

  MatrixDouble &stress = mwlsApprox->stressAtGaussPts;
  stress.resize(6, nb_gauss_pts, false);
  MatrixDouble &diff_stress = mwlsApprox->diffStressAtGaussPts;
  diff_stress.resize(18, nb_gauss_pts, false);

  auto t_mwls_material_positions =
      getFTensor1FromMat<3>(mwlsApprox->mwlsMaterialPositions);
  auto t_approx_base_point = getFTensor1FromMat<3>(mwlsApprox->approxBasePoint);
  for (int gg = 0; gg != nb_gauss_pts; ++gg) {

    auto t_material_positions = getFTensor1FromMat<3>(*matPosAtPtsPtr);

    FTensor::Tensor1<double, 3> t_mat_pos;
    t_mat_pos(i) = t_mwls_material_positions(i);
    FTensor::Tensor1<double, 3> t_approx_base_pos;
    t_approx_base_pos(i) = t_approx_base_point(i);

    if (testing) {
      cerr << "material_positions " << t_mwls_material_positions << endl;
      cerr << "approx_base_point " << t_approx_base_point << endl;
    }
    CHKERR mwlsApprox->getInfluenceNodes(&t_approx_base_pos(0));
    CHKERR mwlsApprox->calculateApproxCoeff(true, use_global_base);
    CHKERR mwlsApprox->evaluateApproxFun(&t_mat_pos(0));
    CHKERR mwlsApprox->evaluateFirstDiffApproxFun(&t_mat_pos(0),
                                                  use_global_base);

    CHKERR mwlsApprox->getTagData(th);
    const auto &vals = mwlsApprox->getDataApprox();

    for (int ii = 0; ii != 6; ++ii)
      stress(ii, gg) = vals[ii];
    if (calculateDerivative) {
      const auto &diff_vals = mwlsApprox->getDiffDataApprox();
      for (int ii = 0; ii != 6; ++ii) {
        for (int jj = 0; jj != 3; ++jj)
          diff_stress(jj * 6 + ii, gg) = diff_vals(jj, ii);
      }
    }
    ++t_mwls_material_positions;
    ++t_approx_base_point;
  }

  if (testing) {
    cerr << "stress " << stress << endl;
    cerr << endl;
  }

  MoFEMFunctionReturn(0);
}

PetscErrorCode MWLSApprox::OpMWLSStressPostProcess::doWork(
    int side, EntityType type, DataForcesAndSourcesCore::EntData &data) {
  MoFEMFunctionBegin;
  if (type != MBVERTEX) {
    MoFEMFunctionReturnHot(0);
  }
  auto &mwls_approx = *mwlsApproxPtr;
  if (mwls_approx.tetsInBlock.find(getFEEntityHandle()) ==
      mwls_approx.tetsInBlock.end()) {
    MoFEMFunctionReturnHot(0);
  }

  auto &post_proc_mesh = *postProcMeshPtr;
  const auto &map_gauss_pts = *mapGaussPtsPtr;
  const auto &stress = mwls_approx.stressAtGaussPts;

  VectorDouble9 stress_tag(9);
  stress_tag.clear();

  Tag tag_stress;
  CHKERR post_proc_mesh.tag_get_handle("STRESS_APPROX", 9, MB_TYPE_DOUBLE,
                                       tag_stress, MB_TAG_CREAT | MB_TAG_SPARSE,
                                       &*stress_tag.begin());

  const int nb_gauss_pts = data.getN().size1();
  for (int gg = 0; gg != nb_gauss_pts; gg++) {
    const EntityHandle post_proc_node = map_gauss_pts[gg];
    for (int ii = 0; ii != 6; ++ii)
      stress_tag[ii] = stress(ii, gg);
    // this can be confusing while postprocessing since paraview by default is
    // showing the magnitude of a tensor
    CHKERR post_proc_mesh.tag_set_data(tag_stress, &post_proc_node, 1,
                                       &*stress_tag.begin());
  }
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MWLSApprox::OpMWLSStressAndErrorsAtGaussPts::doWork(
    int side, EntityType type, DataForcesAndSourcesCore::EntData &data) {
  MoFEMFunctionBegin;

  if (type != MBVERTEX)
    MoFEMFunctionReturnHot(0);

  FTensor::Index<'i', 3> i;
  FTensor::Index<'j', 3> j;

  auto &mwls_approx = *mwlsApproxPtr;
  if (mwls_approx.tetsInBlock.find(getFEEntityHandle()) ==
      mwls_approx.tetsInBlock.end()) {
    MoFEMFunctionReturnHot(0);
  }

  const int nb_gauss_pts = data.getN().size1();
  // SIXX 0 (00) SIYY 1 (11) SIZZ 2 (22) SIXY 3 (01) SIXZ 4 (02) SIYZ 5 (12)
  // T* d00, T* d01, T* d02, T* d11, T* d12, T* d22
  MatrixDouble &stress = mwlsApproxPtr->stressAtGaussPts;
  FTensor::Tensor2_symmetric<FTensor::PackPtr<double *, 1>, 3> t_stress(
      &stress(0, 0), &stress(3, 0), &stress(4, 0), &stress(1, 0), &stress(5, 0),
      &stress(2, 0));

  auto get_symm_tensor = [](auto &m) {
    FTensor::Tensor2_symmetric<double *, 3> t(

        &m[0], &m[3], &m[4],

        &m[1], &m[5],

        &m[2]

    );
    return t;
  };

  auto get_stress_nrm2 = [i, j](auto &t_s) { return t_s(i, j) * t_s(i, j); };

  VectorDouble val_at_nearest_influence_node;
  val_at_nearest_influence_node.resize(9, false);
  val_at_nearest_influence_node.clear();

  Tag th;
  CHKERR mwls_approx.mwlsMoab.tag_get_handle(stressTagName.c_str(), th);

  // This is nasty pice of code, but good for tests. It test value in the
  // nearest mode. We should use base functions to evaluate stress at
  // arbitrary point. However this is good enough when projected mesh and
  // approximated mesh is the same. That is used for tests.
  CHKERR mwls_approx.mwlsMoab.tag_get_data(
      th, &mwls_approx.nearestInfluenceNode, 1,
      &*val_at_nearest_influence_node.data().begin());
  auto t_stress_at_nearset_node =
      get_symm_tensor(val_at_nearest_influence_node);

  auto t_w = getFTensor0IntegrationWeight();
  auto vol = getVolume();

  double int_stress = vol * get_stress_nrm2(t_stress_at_nearset_node);
  double stress_error = 0;
  double hydrostatic_error = 0;
  double deviatoric_error = 0;

  for (int gg = 0; gg != nb_gauss_pts; ++gg) {
    const double a = t_w * vol;

    FTensor::Tensor2_symmetric<double, 3> t_diff_stress;
    t_diff_stress(i, j) = t_stress(i, j) - t_stress_at_nearset_node(i, j);
    stress_error += a * get_stress_nrm2(t_diff_stress);

    constexpr auto t_kd = FTensor::Kronecker_Delta_symmetric<int>();
    // calculate error of the hydro static part of the stress
    const double hydrostatic_error_at_gg =
        (t_kd(i, j) * t_diff_stress(i, j)) / 3.;
    hydrostatic_error += a * hydrostatic_error_at_gg * hydrostatic_error_at_gg;

    // calculate error of the deviator part of the stress
    auto t_dev_error = FTensor::Tensor2_symmetric<double, 3>();
    t_dev_error(i, j) = t_diff_stress(i, j) - hydrostatic_error * t_kd(i, j);
    deviatoric_error += a * get_stress_nrm2(t_dev_error);

    ++t_w;
    ++t_stress;
  }

  auto create_tag = [&](auto name) {
    constexpr double zero = 0;
    Tag tag;
    CHKERR mField.get_moab().tag_get_handle(
        name, 1, MB_TYPE_DOUBLE, tag, MB_TAG_CREAT | MB_TAG_SPARSE, &zero);
    return tag;
  };

  auto save_val = [&](auto &&tag, auto &val) {
    MoFEMFunctionBegin;
    auto const fe_ent = getNumeredEntFiniteElementPtr()->getEnt();
    CHKERR mField.get_moab().tag_set_data(tag, &fe_ent, 1, &val);
    MoFEMFunctionReturn(0);
  };

  stress_error /= int_stress;
  hydrostatic_error /= int_stress;
  deviatoric_error /= int_stress;
  stress_error = sqrt(stress_error);
  hydrostatic_error = sqrt(hydrostatic_error);
  deviatoric_error = sqrt(deviatoric_error);

  CHKERR save_val(create_tag("INT_STRESS_ERROR"), stress_error);
  CHKERR save_val(create_tag("INT_HYDROSTATIC_ERROR"), hydrostatic_error);
  CHKERR save_val(create_tag("INT_DEVIATORIC_ERROR"), deviatoric_error);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MWLSApprox::OpMWLSSpatialStressRhs::doWork(
    int side, EntityType type, DataForcesAndSourcesCore::EntData &data) {
  MoFEMFunctionBegin;
  if (mwlsApprox->tetsInBlock.find(getFEEntityHandle()) ==
      mwlsApprox->tetsInBlock.end()) {
    MoFEMFunctionReturnHot(0);
  }
  const int nb_dofs = data.getIndices().size();
  if (!nb_dofs) {
    MoFEMFunctionReturnHot(0);
  }
  auto t_diff_n = data.getFTensor1DiffN<3>();
  // SIXX 0 (00) SIYY 1 (11) SIZZ 2 (22) SIXY 3 (01) SIXZ 4 (02) SIYZ 5 (12)
  // T* d00, T* d01, T* d02, T* d11, T* d12, T* d22
  MatrixDouble &stress = mwlsApprox->stressAtGaussPts;
  FTensor::Tensor2_symmetric<double *, 3> t_stress(
      &stress(0, 0), &stress(3, 0), &stress(4, 0), &stress(1, 0), &stress(5, 0),
      &stress(2, 0));
  FTensor::Index<'i', 3> i;
  FTensor::Index<'j', 3> j;
  nF.resize(nb_dofs, false);
  nF.clear();
  const int nb_gauss_pts = data.getN().size1();
  auto t_H = getFTensor2FromMat<3, 3>(*matGradPosAtPtsPtr);
  for (int gg = 0; gg != nb_gauss_pts; ++gg) {
    double det;
    CHKERR determinantTensor3by3(t_H, det);
    FTensor::Tensor2<double, 3, 3> t_inv_H;
    CHKERR invertTensor3by3(t_H, det, t_inv_H);
    if (testing) {
      cerr << "V*det " << gg << " " << det * getVolume() << endl;
    }
    double val = getVolume() * getGaussPts()(3, gg) * det;
    FTensor::Tensor1<FTensor::PackPtr<double *, 3>, 3> rhs(&nF[0], &nF[1],
                                                           &nF[2]);
    int bb = 0;
    for (; bb != nb_dofs / 3; ++bb) {
      FTensor::Tensor1<double, 3> t_current_diff_n;
      t_current_diff_n(j) = t_inv_H(i, j) * t_diff_n(i);
      rhs(i) += val * t_stress(i, j) * t_current_diff_n(j);
      ++t_diff_n;
      ++rhs;
    }
    for (; bb != data.getN().size2(); ++bb) {
      ++t_diff_n;
    }
    ++t_stress;
    ++t_H;
  }
  Vec f = mwlsApprox->F_lambda != PETSC_NULL ? mwlsApprox->F_lambda
                                             : getFEMethod()->snes_f;
  if (mwlsApprox->F_lambda == PETSC_NULL) {
    if (auto arc_dof = mwlsApprox->arcLengthDof.lock()) {
      nF *= arc_dof->getFieldData();
    }
  }
  if (testing)
    cerr << "Element Spatial " << getFEEntityHandle() << " " << nF << endl;
  else {
    CHKERR VecSetValues(f, nb_dofs, &data.getIndices()[0], &nF[0], ADD_VALUES);
  }
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MWLSApprox::OpMWLSMaterialStressRhs::doWork(
    int side, EntityType type, DataForcesAndSourcesCore::EntData &data) {
  MoFEMFunctionBegin;
  if (mwlsApprox->tetsInBlock.find(getFEEntityHandle()) ==
      mwlsApprox->tetsInBlock.end()) {
    MoFEMFunctionReturnHot(0);
  }
  const int nb_dofs = data.getIndices().size();
  if (!nb_dofs) {
    MoFEMFunctionReturnHot(0);
  }
  auto t_diff_n = data.getFTensor1DiffN<3>();
  // SIXX 0 (00) SIYY 1 (11) SIZZ 2 (22) SIXY 3 (01) SIXZ 4 (02) SIYZ 5 (12)
  // T* d00, T* d01, T* d02, T* d11, T* d12, T* d22
  MatrixDouble &stress = mwlsApprox->stressAtGaussPts;
  FTensor::Tensor2_symmetric<FTensor::PackPtr<double *, 1>, 3> t_stress(
      &stress(0, 0), &stress(3, 0), &stress(4, 0), &stress(1, 0), &stress(5, 0),
      &stress(2, 0));
  FTensor::Index<'i', 3> i;
  FTensor::Index<'j', 3> j;
  FTensor::Index<'k', 3> k;

  nF.resize(nb_dofs, false);
  nF.clear();
  const int nb_gauss_pts = data.getN().size1();
  auto t_h = getFTensor2FromMat<3, 3>(*spaceGradPosAtPtsPtr);
  auto t_H = getFTensor2FromMat<3, 3>(*matGradPosAtPtsPtr);
  for (int gg = 0; gg != nb_gauss_pts; ++gg) {
    double det;
    CHKERR determinantTensor3by3(t_H, det);
    FTensor::Tensor2<double, 3, 3> t_inv_H;
    CHKERR invertTensor3by3(t_H, det, t_inv_H);
    double val = getVolume() * getGaussPts()(3, gg) * det;

    FTensor::Tensor2<double, 3, 3> t_F;
    // Calculate gradient of deformation
    t_F(i, k) = t_h(i, j) * t_inv_H(j, k);

    constexpr auto t_kd = FTensor::Kronecker_Delta<int>();
    FTensor::Tensor2<double, 3, 3> t_eshelby_stress;
    t_eshelby_stress(i, k) = -t_F(j, i) * t_stress(j, k);
    int bb = 0;
    FTensor::Tensor1<double *, 3> rhs(&nF[0], &nF[1], &nF[2], 3);
    for (; bb != nb_dofs / 3; ++bb) {
      FTensor::Tensor1<double, 3> t_current_diff_n;
      t_current_diff_n(j) = t_inv_H(i, j) * t_diff_n(i);
      rhs(i) += val * t_eshelby_stress(i, j) * t_current_diff_n(j);
      ++t_diff_n;
      ++rhs;
    }
    for (; bb != data.getN().size2(); ++bb) {
      ++t_diff_n;
    }
    ++t_stress;
    ++t_H;
    ++t_h;
  }
  int *indices_ptr = &data.getIndices()[0];
  if (!forcesOnlyOnEntitiesRow.empty()) {
    iNdices.resize(nb_dofs);
    noalias(iNdices) = data.getIndices();
    indices_ptr = &iNdices[0];
    auto &dofs = data.getFieldDofs();
    auto dit = dofs.begin();
    for (int ii = 0; dit != dofs.end(); dit++, ii++) {
      if (auto dof = (*dit)) {
        if (forcesOnlyOnEntitiesRow.find(dof->getEnt()) ==
            forcesOnlyOnEntitiesRow.end()) {
          iNdices[ii] = -1;
        }
      }
    }
  }
  Vec f = F_lambda != PETSC_NULL ? F_lambda : getFEMethod()->snes_f;
  if (mwlsApprox->F_lambda == PETSC_NULL) {
    if (auto arc_dof = mwlsApprox->arcLengthDof.lock()) {
      nF *= arc_dof->getFieldData();
    }
  }
  CHKERR VecSetOption(f, VEC_IGNORE_NEGATIVE_INDICES, PETSC_TRUE);
  CHKERR VecSetValues(f, nb_dofs, indices_ptr, &nF[0], ADD_VALUES);
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MWLSApprox::OpMWLSSpatialStressLhs_DX::doWork(
    int row_side, int col_side, EntityType row_type, EntityType col_type,
    DataForcesAndSourcesCore::EntData &row_data,
    DataForcesAndSourcesCore::EntData &col_data) {
  MoFEMFunctionBegin;
  if (mwlsApprox->tetsInBlock.find(getFEEntityHandle()) ==
      mwlsApprox->tetsInBlock.end()) {
    MoFEMFunctionReturnHot(0);
  }
  const int row_nb_dofs = row_data.getIndices().size();
  if (!row_nb_dofs) {
    MoFEMFunctionReturnHot(0);
  }
  const int col_nb_dofs = col_data.getIndices().size();
  if (!col_nb_dofs) {
    MoFEMFunctionReturnHot(0);
  }
  nA.resize(row_nb_dofs, col_nb_dofs, false);
  nA.clear();

  // SIXX 0 (00) SIYY 1 (11) SIZZ 2 (22) SIXY 3 (01) SIXZ 4 (02) SIYZ 5 (12)
  MatrixDouble &stress = mwlsApprox->stressAtGaussPts;
  FTensor::Tensor2_symmetric<FTensor::PackPtr<double *, 1>, 3> t_stress(
      &stress(0, 0), &stress(3, 0), &stress(4, 0), &stress(1, 0), &stress(5, 0),
      &stress(2, 0));
  MatrixDouble &d_stress = mwlsApprox->diffStressAtGaussPts;
  // Note Christod is symmetric on first two indices
  FTensor::Christof<FTensor::PackPtr<double *, 1>, 3, 3> t_d_stress(
      &d_stress(0 * 6 + 0, 0), &d_stress(0 * 6 + 3, 0), &d_stress(0 * 6 + 4, 0),
      &d_stress(0 * 6 + 1, 0), &d_stress(0 * 6 + 5, 0), &d_stress(0 * 6 + 2, 0),

      &d_stress(1 * 6 + 0, 0), &d_stress(1 * 6 + 3, 0), &d_stress(1 * 6 + 4, 0),
      &d_stress(1 * 6 + 1, 0), &d_stress(1 * 6 + 5, 0), &d_stress(1 * 6 + 2, 0),

      &d_stress(2 * 6 + 0, 0), &d_stress(2 * 6 + 3, 0), &d_stress(2 * 6 + 4, 0),
      &d_stress(2 * 6 + 1, 0), &d_stress(2 * 6 + 5, 0), &d_stress(2 * 6 + 2, 0)

  );

  // scale stress
  double lambda = 1;
  if (auto arc_dof = mwlsApprox->arcLengthDof.lock()) {
    lambda = arc_dof->getFieldData();
  }

  auto t_row_diff_n = row_data.getFTensor1DiffN<3>();
  const int nb_row_base = row_data.getN().size2();
  const int nb_gauss_pts = row_data.getN().size1();
  auto t_initial_singular_displacement =
      getFTensor1FromMat<3>(mwlsApprox->singularInitialDisplacement);
  auto t_H = getFTensor2FromMat<3, 3>(*matGradPosAtPtsPtr);

  FTensor::Index<'i', 3> i;
  FTensor::Index<'j', 3> j;
  FTensor::Index<'k', 3> k;
  FTensor::Index<'m', 3> m;
  FTensor::Index<'n', 3> n;

  for (int gg = 0; gg != nb_gauss_pts; ++gg) {

    double det;
    CHKERR determinantTensor3by3(t_H, det);
    FTensor::Tensor2<double, 3, 3> t_inv_H;
    CHKERR invertTensor3by3(t_H, det, t_inv_H);

    double val = getVolume() * getGaussPts()(3, gg) * det;
    int rr = 0;
    for (; rr != row_nb_dofs / 3; ++rr) {
      FTensor::Tensor1<double, 3> t_current_row_diff_n;
      t_current_row_diff_n(j) = t_inv_H(i, j) * t_row_diff_n(i);
      auto t_col_diff_n = col_data.getFTensor1DiffN<3>(gg, 0);
      auto t_col_n = col_data.getFTensor0N(gg, 0);
      FTensor::Tensor2<FTensor::PackPtr<double *, 3>, 3, 3> t_mat(
          &nA(3 * rr + 0, 0), &nA(3 * rr + 0, 1), &nA(3 * rr + 0, 2),
          &nA(3 * rr + 1, 0), &nA(3 * rr + 1, 1), &nA(3 * rr + 1, 2),
          &nA(3 * rr + 2, 0), &nA(3 * rr + 2, 1), &nA(3 * rr + 2, 2));
      FTensor::Tensor1<double, 3> t_a;
      t_a(i) = val * lambda * t_stress(i, j) * t_current_row_diff_n(j);
      FTensor::Tensor2<double, 3, 3> t_b;
      t_b(i, j) = val * lambda * t_d_stress(i, j, k) * t_current_row_diff_n(k);
      FTensor::Tensor3<double, 3, 3, 3> t_c;
      t_c(i, m, n) = -val * lambda * (t_stress(i, j) * t_inv_H(n, j)) *
                     (t_inv_H(k, m) * t_row_diff_n(k));
      for (int cc = 0; cc != col_nb_dofs / 3; ++cc) {
        t_mat(i, j) += t_a(i) * t_inv_H(k, j) * t_col_diff_n(k);
        t_mat(i, j) += t_b(j, i) * t_col_n;
        t_mat(i, j) +=
            t_b(j, i) * t_col_diff_n(k) * t_initial_singular_displacement(k);
        t_mat(i, m) += t_c(i, m, n) * t_col_diff_n(n);
        ++t_col_n;
        ++t_col_diff_n;
        ++t_mat;
      }
      ++t_row_diff_n;
    }
    for (; rr != nb_row_base; ++rr) {
      ++t_row_diff_n;
    }

    // for (int ii = 0; ii != 3; ++ii)
    //   for (int kk = 0; kk != 3; ++kk)
    //     for (int ll = kk + 1; ll < 3; ++ll)
    //       if (t_d_stress(ii, kk, ll) != t_d_stress(ii, ll, kk))
    // SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "AAA");

    ++t_H;
    ++t_stress;
    ++t_d_stress;
    ++t_initial_singular_displacement;
  }
  CHKERR MatSetValues(getFEMethod()->snes_B, row_nb_dofs,
                      &*row_data.getIndices().begin(), col_nb_dofs,
                      &*col_data.getIndices().begin(), &*nA.data().begin(),
                      ADD_VALUES);
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MWLSApprox::OpMWLSMaterialStressLhs_Dx::doWork(
    int row_side, int col_side, EntityType row_type, EntityType col_type,
    DataForcesAndSourcesCore::EntData &row_data,
    DataForcesAndSourcesCore::EntData &col_data) {
  MoFEMFunctionBegin;
  if (mwlsApprox->tetsInBlock.find(getFEEntityHandle()) ==
      mwlsApprox->tetsInBlock.end()) {
    MoFEMFunctionReturnHot(0);
  }
  const int row_nb_dofs = row_data.getIndices().size();
  if (!row_nb_dofs) {
    MoFEMFunctionReturnHot(0);
  }
  const int col_nb_dofs = col_data.getIndices().size();
  if (!col_nb_dofs) {
    MoFEMFunctionReturnHot(0);
  }
  nA.resize(row_nb_dofs, col_nb_dofs, false);
  nA.clear();
  MatrixDouble &stress = mwlsApprox->stressAtGaussPts;
  FTensor::Tensor2_symmetric<FTensor::PackPtr<double *, 1>, 3> t_stress(
      &stress(0, 0), &stress(3, 0), &stress(4, 0), &stress(1, 0), &stress(5, 0),
      &stress(2, 0));

  auto t_row_diff_n = row_data.getFTensor1DiffN<3>();
  const int nb_row_base = row_data.getN().size2();
  const int nb_gauss_pts = row_data.getN().size1();
  auto t_H = getFTensor2FromMat<3, 3>(*matGradPosAtPtsPtr);
  auto t_h = getFTensor2FromMat<3, 3>(*spaceGradPosAtPtsPtr);

  FTensor::Index<'i', 3> i;
  FTensor::Index<'j', 3> j;
  FTensor::Index<'k', 3> k;
  FTensor::Index<'l', 3> l;
  FTensor::Index<'m', 3> m;
  FTensor::Index<'n', 3> n;

  double lambda = 1;
  if (auto arc_dof = mwlsApprox->arcLengthDof.lock()) {
    lambda = arc_dof->getFieldData();
  }

  for (int gg = 0; gg != nb_gauss_pts; ++gg) {

    double det;
    CHKERR determinantTensor3by3(t_H, det);
    FTensor::Tensor2<double, 3, 3> t_inv_H;
    CHKERR invertTensor3by3(t_H, det, t_inv_H);
    double val = getVolume() * getGaussPts()(3, gg) * det;

    int rr = 0;
    for (; rr != row_nb_dofs / 3; ++rr) {
      auto t_col_diff_n = col_data.getFTensor1DiffN<3>(gg, 0);
      FTensor::Tensor1<double, 3> t_current_row_diff_n;
      t_current_row_diff_n(j) = t_inv_H(i, j) * t_row_diff_n(i);
      FTensor::Tensor2<double *, 3, 3> t_mat(
          &nA(3 * rr + 0, 0), &nA(3 * rr + 0, 1), &nA(3 * rr + 0, 2),
          &nA(3 * rr + 1, 0), &nA(3 * rr + 1, 1), &nA(3 * rr + 1, 2),
          &nA(3 * rr + 2, 0), &nA(3 * rr + 2, 1), &nA(3 * rr + 2, 2), 3);
      FTensor::Tensor1<double, 3> t_a;
      t_a(j) = -val * lambda * t_stress(j, k) * t_current_row_diff_n(k);
      int cc = 0;
      for (; cc != col_nb_dofs / 3; cc++) {
        t_mat(i, j) += t_inv_H(l, i) * t_a(j) * t_col_diff_n(l);
        ++t_col_diff_n;
        ++t_mat;
      }
      ++t_row_diff_n;
    }
    for (; rr != nb_row_base; ++rr) {
      ++t_row_diff_n;
    }
    ++t_stress;
    ++t_H;
    // ++t_h;
  }
  int *indices_ptr = &row_data.getIndices()[0];
  if (!forcesOnlyOnEntitiesRow.empty()) {
    iNdices.resize(row_nb_dofs);
    noalias(iNdices) = row_data.getIndices();
    indices_ptr = &iNdices[0];
    auto &dofs = row_data.getFieldDofs();
    auto dit = dofs.begin();
    for (int ii = 0; dit != dofs.end(); dit++, ii++) {
      if (auto dof = (*dit)) {
        if (forcesOnlyOnEntitiesRow.find(dof->getEnt()) ==
            forcesOnlyOnEntitiesRow.end()) {
          iNdices[ii] = -1;
        }
      }
    }
  }
  CHKERR MatSetValues(getFEMethod()->snes_B, row_nb_dofs, indices_ptr,
                      col_nb_dofs, &*col_data.getIndices().begin(),
                      &*nA.data().begin(), ADD_VALUES);
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode MWLSApprox::OpMWLSMaterialStressLhs_DX::doWork(
    int row_side, int col_side, EntityType row_type, EntityType col_type,
    DataForcesAndSourcesCore::EntData &row_data,
    DataForcesAndSourcesCore::EntData &col_data) {
  MoFEMFunctionBegin;
  if (mwlsApprox->tetsInBlock.find(getFEEntityHandle()) ==
      mwlsApprox->tetsInBlock.end()) {
    MoFEMFunctionReturnHot(0);
  }
  const int row_nb_dofs = row_data.getIndices().size();
  if (!row_nb_dofs) {
    MoFEMFunctionReturnHot(0);
  }
  const int col_nb_dofs = col_data.getIndices().size();
  if (!col_nb_dofs) {
    MoFEMFunctionReturnHot(0);
  }
  nA.resize(row_nb_dofs, col_nb_dofs, false);
  nA.clear();
  MatrixDouble &stress = mwlsApprox->stressAtGaussPts;
  // SIXX 0 (00) SIYY 1 (11) SIZZ 2 (22) SIXY 3 (01) SIXZ 4 (02) SIYZ 5 (12)
  FTensor::Tensor2<FTensor::PackPtr<double *, 1>, 3, 3> t_stress(
      &stress(0, 0), &stress(3, 0), &stress(4, 0), &stress(3, 0), &stress(1, 0),
      &stress(5, 0), &stress(4, 0), &stress(5, 0), &stress(2, 0));
  MatrixDouble &d_stress = mwlsApprox->diffStressAtGaussPts;
  FTensor::Tensor3<FTensor::PackPtr<double *, 1>, 3, 3, 3> t_d_stress(
      &d_stress(0 * 6 + 0, 0), &d_stress(0 * 6 + 3, 0), &d_stress(0 * 6 + 4, 0),
      &d_stress(0 * 6 + 3, 0), &d_stress(0 * 6 + 1, 0), &d_stress(0 * 6 + 5, 0),
      &d_stress(0 * 6 + 4, 0), &d_stress(0 * 6 + 5, 0), &d_stress(0 * 6 + 2, 0),

      &d_stress(1 * 6 + 0, 0), &d_stress(1 * 6 + 3, 0), &d_stress(1 * 6 + 4, 0),
      &d_stress(1 * 6 + 3, 0), &d_stress(1 * 6 + 1, 0), &d_stress(1 * 6 + 5, 0),
      &d_stress(1 * 6 + 4, 0), &d_stress(1 * 6 + 5, 0), &d_stress(1 * 6 + 2, 0),

      &d_stress(2 * 6 + 0, 0), &d_stress(2 * 6 + 3, 0), &d_stress(2 * 6 + 4, 0),
      &d_stress(2 * 6 + 3, 0), &d_stress(2 * 6 + 1, 0), &d_stress(2 * 6 + 5, 0),
      &d_stress(2 * 6 + 4, 0), &d_stress(2 * 6 + 5, 0), &d_stress(2 * 6 + 2, 0)

  );
  auto t_initial_singular_displacement =
      getFTensor1FromMat<3>(mwlsApprox->singularInitialDisplacement);

  auto t_row_diff_n = row_data.getFTensor1DiffN<3>();
  const int nb_row_base = row_data.getN().size2();
  const int nb_gauss_pts = row_data.getN().size1();
  auto t_H = getFTensor2FromMat<3, 3>(*matGradPosAtPtsPtr);
  auto t_h = getFTensor2FromMat<3, 3>(*spaceGradPosAtPtsPtr);

  FTensor::Index<'i', 3> i;
  FTensor::Index<'j', 3> j;
  FTensor::Index<'k', 3> k;
  FTensor::Index<'l', 3> l;
  FTensor::Index<'m', 3> m;
  FTensor::Index<'n', 3> n;

  double lambda = 1;
  if (auto arc_dof = mwlsApprox->arcLengthDof.lock()) {
    lambda = arc_dof->getFieldData();
  }

  for (int gg = 0; gg != nb_gauss_pts; ++gg) {

    double det;
    CHKERR determinantTensor3by3(t_H, det);
    FTensor::Tensor2<double, 3, 3> t_inv_H;
    CHKERR invertTensor3by3(t_H, det, t_inv_H);
    double val = getVolume() * getGaussPts()(3, gg) * det;

    FTensor::Tensor2<double, 3, 3> t_F;
    t_F(i, j) = t_h(i, k) * t_inv_H(k, j);
    FTensor::Tensor3<double, 3, 3, 3> t_a;
    t_a(i, k, l) = -val * lambda * t_F(j, i) * t_d_stress(l, j, k);
    FTensor::Tensor4<double, 3, 3, 3, 3> t_b;
    t_b(i, k, m, n) = val * lambda * t_h(j, l) * t_inv_H(l, m) * t_inv_H(n, i) *
                      t_stress(j, k);
    FTensor::Tensor2<double, 3, 3> t_c;
    t_c(i, k) = -val * lambda * t_F(j, i) * t_stress(j, k);
    int rr = 0;
    for (; rr != row_nb_dofs / 3; ++rr) {
      FTensor::Tensor1<double, 3> t_current_row_diff_n;
      t_current_row_diff_n(j) = t_inv_H(i, j) * t_row_diff_n(i);
      auto t_col_diff_n = col_data.getFTensor1DiffN<3>(gg, 0);
      auto t_col_n = col_data.getFTensor0N(gg, 0);
      FTensor::Tensor2<double *, 3, 3> t_mat(
          &nA(3 * rr + 0, 0), &nA(3 * rr + 0, 1), &nA(3 * rr + 0, 2),
          &nA(3 * rr + 1, 0), &nA(3 * rr + 1, 1), &nA(3 * rr + 1, 2),
          &nA(3 * rr + 2, 0), &nA(3 * rr + 2, 1), &nA(3 * rr + 2, 2), 3);
      FTensor::Tensor2<double, 3, 3> t_a_a;
      t_a_a(i, l) = t_a(i, k, l) * t_current_row_diff_n(k);
      FTensor::Tensor3<double, 3, 3, 3> t_b_b;
      t_b_b(i, m, n) = t_b(i, k, m, n) * t_current_row_diff_n(k);
      FTensor::Tensor1<double, 3> t_c_c0;
      t_c_c0(i) = t_c(i, k) * t_current_row_diff_n(k);
      FTensor::Tensor3<double, 3, 3, 3> t_c_c1;
      t_c_c1(i, m, n) =
          -t_c(i, l) * t_inv_H(k, m) * t_inv_H(n, l) * t_row_diff_n(k);
      int cc = 0;
      for (; cc != col_nb_dofs / 3; cc++) {
        t_mat(i, l) += t_a_a(i, l) * t_col_n;
        t_mat(i, l) +=
            t_a_a(i, l) * t_col_diff_n(k) * t_initial_singular_displacement(k);
        t_mat(i, m) += t_b_b(i, m, n) * t_col_diff_n(n);
        t_mat(i, m) += t_c_c0(i) * t_inv_H(n, m) * t_col_diff_n(n);
        t_mat(i, m) += t_c_c1(i, m, n) * t_col_diff_n(n);
        ++t_col_diff_n;
        ++t_col_n;
        ++t_mat;
      }
      ++t_row_diff_n;
    }
    for (; rr != nb_row_base; ++rr) {
      ++t_row_diff_n;
    }
    ++t_stress;
    ++t_d_stress;
    ++t_initial_singular_displacement;
    ++t_H;
    ++t_h;
  }
  int *indices_ptr = &row_data.getIndices()[0];
  if (!forcesOnlyOnEntitiesRow.empty()) {
    iNdices.resize(row_nb_dofs);
    noalias(iNdices) = row_data.getIndices();
    indices_ptr = &iNdices[0];
    auto &dofs = row_data.getFieldDofs();
    auto dit = dofs.begin();
    for (int ii = 0; dit != dofs.end(); dit++, ii++) {
      if (auto dof = (*dit)) {
        if (forcesOnlyOnEntitiesRow.find(dof->getEnt()) ==
            forcesOnlyOnEntitiesRow.end()) {
          iNdices[ii] = -1;
        }
      }
    }
  }
  CHKERR MatSetValues(getFEMethod()->snes_B, row_nb_dofs, indices_ptr,
                      col_nb_dofs, &*col_data.getIndices().begin(),
                      &*nA.data().begin(), ADD_VALUES);
  MoFEMFunctionReturn(0);
}
} // namespace FractureMechanics